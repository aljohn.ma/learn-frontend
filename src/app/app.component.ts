import { Component } from '@angular/core';
import { TokenService } from './services/token.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'learning-app';
  is_logged_in = false;

  constructor(private token: TokenService) {
    token.token.subscribe(
      par => {
        this.is_logged_in = token.is_valid(par as string);
      }
    );
  }

}
