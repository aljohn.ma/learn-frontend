import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './components/main/main.component';
import { UsersComponent } from './components/users/users.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { LoginComponent } from './components/forms/login/login.component';
import { RecoverComponent } from './components/forms/recover/recover.component';
import { UserFormComponent } from './components/users/user-form/user-form.component';
import { StudentComponent } from './components/users/student/student.component';
import { AllUsersComponent } from './components/users/all-users/all-users.component';
import { TeachersComponent } from './components/users/teachers/teachers.component';
import { StaffComponent } from './components/users/staff/staff.component';
import { ParentsComponent } from './components/users/parents/parents.component';
import { AddUserComponent } from './components/users/add-user/add-user.component';
import { GrantPageAccessService } from './services/grant-access.service';
import { UnauthorizedComponent } from './components/unauthorized/unauthorized.component';
import { EditUserComponent } from './components/users/edit-user/edit-user.component';
import { ProfileComponent } from './components/main/profile/profile.component';
import { TeacherModule } from './components/teacher/teacher.module';
import { SetupModule } from './components/setup/setup.module';
import { UsersModule } from './components/users/users.module';
import { StudentModule } from './components/student/student.module';

const routes: Routes = [
  { path: '', component: MainComponent },
  { path: 'login', component: LoginComponent },
  { path: 'recover', component: RecoverComponent },
  { path: 'profile', component: ProfileComponent },
  {
    path: 'users',
    component: UsersComponent,
    children: [
      { path: '', loadChildren: () => UsersModule }
    ],
    canActivate: [GrantPageAccessService]
  },
  { path: 'setup', loadChildren: () => SetupModule },
  { path: 'teacher', loadChildren: () => TeacherModule },
  { path: 'student', loadChildren: () => StudentModule },

  { path: '401', component: UnauthorizedComponent },
  { path: '**', component: PageNotFoundComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [
  MainComponent, LoginComponent,
  RecoverComponent,
  PageNotFoundComponent,
  UnauthorizedComponent,
  UsersComponent,
];
