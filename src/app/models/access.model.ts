export class Access {
    id: number;
    user_id: number;
    feature_id: number;
    can_add: boolean;
    can_delete: boolean;
    can_edit: boolean;

    constructor(
        feature_id: number,
        can_add = false,
        can_edit = false,
        can_delete = false
    ) {
        this.feature_id = feature_id;
        this.can_add = can_add;
        this.can_edit = can_edit;
        this.can_delete = can_delete;
    }
}
