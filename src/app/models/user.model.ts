import { Student } from './student.model';
import { UserConfig } from '../components/users/user.config';
import { Feature } from './feature.model';
import { Guardian } from './guardian.model';
import { USER } from '../constants/constants';

export class User {

    public id: number;
    public first_name: string;
    public middle_name: string;
    public last_name: string;
    public gender: string;

    public email: string;
    public username: string;
    public password: string;
    public password_confirmation: string;

    public birthdate: Date;
    public marital_status: string;

    public mobile_number: string;
    public phone_number: string;

    public address_1: string;
    public address_2: string;
    public address_3: string;

    public country_id = 0;
    public state_id = 0;
    public city_id = 0;
    public postal_code_id = 0;

    public profile_path: any;
    public profile_pic: any;

    public user_type: number;
    public is_active: boolean;

    public student: Student = new Student();
    public parent: Guardian = new Guardian();

    public user_access: Feature[] = new Array<Feature>();
    public removed_access: Feature[] = new Array<Feature>();

    constructor() { }

    getFullname() {
        return this.middle_name == null ?
            `${this.first_name} ${this.last_name}` :
            `${this.first_name} ${this.middle_name.charAt(0).toUpperCase()}. ${this.last_name}`;
    }

    getUserType() {
        return UserConfig.getUsertypeStr(this.user_type);
    }

    getStatusTitle() {
        return this.is_active ? 'Click to disable' : 'Click to enable';
    }

    isTeacher() {
        return this.user_type === USER.TEACHER;
    }

    isParent() {
        return this.user_type === USER.PARENT;
    }

    isStudent() {
        return this.user_type === USER.STUDENT;
    }
}
