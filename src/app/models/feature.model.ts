import { Access } from './access.model';

export class Feature {
    id: number;
    name: string;
    url: string;
    pivot: Access;

    constructor() {
        this.pivot = new Access(this.id);
    }
}
