export class Guardian {
    user_id: number;
    student_id: number = null;
    pg_code: string;
    pg_relationship: string;
}

