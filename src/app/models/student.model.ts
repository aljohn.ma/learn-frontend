

export class Student {
    id: number;
    user_id: number;
    student_code: string;
    student_number: string;
    level_id: number = null;
    section_id: number = null;
    schedule_id: number = null;
    schedule: any = {};
    q_e_hs: any = [];
}
