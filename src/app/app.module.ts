import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

// INSTALLED MODULES
import { NgFlashMessagesModule } from 'ng-flash-messages';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

// PROJECT IMPORTS
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { AppRoutingModule, routingComponents } from './app.routes';
import { ProfileComponent } from './components/main/profile/profile.component';
import { TeacherComponent } from './components/main/dashboard/teacher/teacher.component';
import { StudentComponent } from './components/main/dashboard/student/student.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { SharedModule } from './shared.module';


@NgModule({
  declarations: [
    AppComponent,
    routingComponents,
    HeaderComponent,
    ProfileComponent,
    TeacherComponent,
    StudentComponent,
  ],
  imports: [
    CommonModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    NgbModule,
    NgxDatatableModule,
    NgFlashMessagesModule.forRoot(),
    SharedModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
