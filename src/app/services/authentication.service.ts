import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import {
  LOGIN_USER,
  LOGOUT_USER,
  RECOVER_ACCOUNT,
  RESET_PASSWORD,
  GET_AUTH_USER_ACCESS,
  GET_AUTH_ACCESS
} from '../constants/urls';

import { TokenService } from './token.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import { plainToClass } from 'class-transformer';
import { Access } from '../models/access.model';
import { LoginStatusService } from './login.service';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private helper = new JwtHelperService();

  constructor(
    private router: Router,
    private http: HttpClient,
    private tokenService: TokenService,
    private login_status: LoginStatusService

  ) { }

  authenticate() {
    if (!this.tokenService.is_valid()) {
      this.tokenService.remove();
      this.router.navigate(['/login']);
    }
  }

  loginUser(credentials) {
    return this.http.post(LOGIN_USER(), credentials);
  }

  logoutUser() {
    return this.http.post(LOGOUT_USER(), { 'token': this.tokenService.get() });
  }

  recoverAccount(email) {
    return this.http.post(RECOVER_ACCOUNT(), email);
  }

  changePassword(data) {
    return this.http.post(RESET_PASSWORD(), data);
  }

  getUserAccess() {
    return this.http.post(GET_AUTH_USER_ACCESS(), { 'token': this.tokenService.get() });
  }

  hasPageAccess(url) {

    const access: any = JSON.parse(atob(localStorage.getItem('axs')));
    let ret = false;

    if (access.length > 0) {
      const acc = access.find(e => e.url === url);
      if (acc) {
        ret = Boolean(acc.pivot.can_add || acc.pivot.can_edit || acc.pivot.can_delete);
      }
    }

    return ret;
  }

  getAccess(url) {
    const HEADERS = new HttpHeaders().set('Authorization', 'Bearer ' + localStorage.getItem('token'));
    return this.http.get(GET_AUTH_ACCESS(url), { headers: HEADERS });
  }

}
