export const LOGIN_USER = () => 'api/login';
export const LOGOUT_USER = () => 'api/logout';
export const RECOVER_ACCOUNT = () => 'api/recover';
export const RESET_PASSWORD = () => 'api/reset-password';
export const GET_AUTH_USER = () => 'api/get-user';

export const GET_AUTH_USER_ACCESS = () => 'api/get-auth-user-access';
export const GET_AUTH_ACCESS = (url: string) => `api/get-auth-access/${url}`;

export const API_USERS = () => 'api/users';
export const GET_USERS_BY_TYPE = (user_type) => `api/get-users-by-type/${user_type}`;
export const ENABLE_DISABLE_USER = () => 'api/enable-disable';
export const GET_SECTIONS_BY_LEVEL = () => 'api/get-sections-by-level';
export const GET_SCHEDULES_BY_SECTION = () => 'api/get-schedules-by-section';

export const GET_APP_FEATURES = () => 'api/get-app-features';

export const API_COUNTRIES = () => 'api/countries';
export const GET_STATES = () => 'api/get-states';
export const GET_CITIES = () => 'api/get-cities';
export const GET_POSTAL_CODES = () => 'api/get-postal-codes';


export const SAVE_STATE = () => 'api/save-state';
export const SAVE_CITY = () => 'api/save-city';
export const SAVE_POSTAL = () => 'api/save-postal-code';

export const API_LEVELS = () => 'api/levels';

export const API_SUBJECTS = () => 'api/subjects';

export const API_ROOMS = () => 'api/rooms';

export const API_SECTIONS = () => 'api/sections';

export const API_SCHEDULES = () => 'api/schedules';

export const API_SCHEDULE_LINES = () => 'api/schedule-lines';

export const API_CLASS_POSITIONS = () => 'api/class-positions';

export const GET_AUTH_TEACHER_SUBJECTS = () => 'api/get-auth-teacher-subjects';

export const API_QEH = (extra_url = '') => `api/qeh${extra_url}`;

export const PUBLISH_QEH = () => 'api/publish-qeh';

export const API_QEH_MC = (extra_url = '') => `api/qeh-multiple-choice${extra_url}`;
export const API_QEH_TF = (extra_url = '') => `api/qeh-true-false${extra_url}`;
export const API_QEH_ID = (extra_url = '') => `api/qeh-identification${extra_url}`;
export const API_QEH_EN = (extra_url = '') => `api/qeh-enumeration${extra_url}`;
export const API_QEH_ESS = (extra_url = '') => `api/qeh-essay${extra_url}`;

export const GET_STUDENT_QEH = () => 'api/get-student-qeh';
export const GET_STUDENT_QEH_1 = () => 'api/get-student-qeh-1';
export const START_QEH = () => 'api/start-qeh';
export const SUBMIT_QEH = () => 'api/submit-qeh';
export const GET_STUDENT_QEH_RESULT = () => 'api/get-student-qeh-result';







