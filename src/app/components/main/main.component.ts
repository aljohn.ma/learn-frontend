import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../services/authentication.service';
import { ApiService } from '../../services/api.service';
import { User } from '../../models/user.model';
import { GET_AUTH_USER } from '../../constants/urls';
import { plainToClass } from 'class-transformer';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  user: User;

  constructor(
    private auth: AuthenticationService,
    private http: ApiService,
  ) { }

  async ngOnInit() {
    await this.auth.authenticate();
    this.getUser();
  }

  async getUser() {
    await this.http.get_list(GET_AUTH_USER()).toPromise().then(
      res => {
        this.user = plainToClass(User, res['user'] as User);
      }
    );
  }

}
