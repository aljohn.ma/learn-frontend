import { Component, OnInit } from '@angular/core';
import { User } from '../../../../models/user.model';
import { AuthenticationService } from '../../../../services/authentication.service';
import { ApiService } from '../../../../services/api.service';
import { GET_AUTH_USER } from '../../../../constants/urls';
import { plainToClass } from 'class-transformer';
import { QEH, QEH_CATEGORY } from '../../../../constants/constants';

@Component({
  selector: 'app-teacher-dashboard',
  templateUrl: './teacher.component.html',
  styleUrls: ['./teacher.component.scss']
})
export class TeacherComponent implements OnInit {

  QEH_category = QEH_CATEGORY;
  user: User;
  QEHs: any = [];

  exams: any;
  quizzes: any;
  homeworks: any;
  exercises: any;

  constructor(
    private auth: AuthenticationService,
    private http: ApiService,
  ) { }

  ngOnInit() {
    this.auth.authenticate();
    this.getUser();
  }

  getUser() {
    this.http.get_list(GET_AUTH_USER()).subscribe(
      res => {
        this.user = plainToClass(User, res['user'] as User);
        this.QEHs = res['qehs'];
        this.categorizeQEH();
      }
    );
  }

  categorizeQEH() {
    this.quizzes = this.QEHs.filter(d => d.qeh_type === QEH.QUIZ);
    this.exams = this.QEHs.filter(d => d.qeh_type === QEH.EXAM);
    this.homeworks = this.QEHs.filter(d => d.qeh_type === QEH.HOMEWORK);
    this.exercises = this.QEHs.filter(d => d.qeh_type === QEH.EXERCISE);
  }
}
