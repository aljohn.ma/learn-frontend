import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../../../services/authentication.service';
import { ApiService } from '../../../../services/api.service';
import { User } from '../../../../models/user.model';
import { GET_AUTH_USER } from '../../../../constants/urls';
import { plainToClass } from 'class-transformer';
import { QEH, getQEHLength } from '../../../../constants/constants';

@Component({
  selector: 'app-student-dashboard',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.scss']
})
export class StudentComponent implements OnInit {
  QEH_type = QEH;
  user: User;

  loading = true;

  QEHs: any;
  QEHs_available: any;

  exams: any;
  quizzes: any;
  homeworks: any;
  exercises: any;

  constructor(
    private auth: AuthenticationService,
    private http: ApiService,
  ) { }

  ngOnInit() {
    this.auth.authenticate();
    this.getUser();
  }

  getUser() {
    this.http.get_list(GET_AUTH_USER()).subscribe(
      res => {
        this.user = plainToClass(User, res['user'] as User);
        this.user.student = res['student_info'];
        this.categorizeQEH();
      },
      () => {
        this.loading = false;
      }
    );
  }

  categorizeQEH() {
    this.QEHs = this.user.student.q_e_hs;
    this.QEHs_available = this.QEHs.filter(d => d.status !== 3);

    this.quizzes = this.QEHs.filter(d => d.qeh_type === QEH.QUIZ);
    this.exams = this.QEHs.filter(d => d.qeh_type === QEH.EXAM);
    this.homeworks = this.QEHs.filter(d => d.qeh_type === QEH.HOMEWORK);
    this.exercises = this.QEHs.filter(d => d.qeh_type === QEH.EXERCISE);

    this.loading = false;
  }

  getAvailableQEH(type: QEH) {
    const ret = this.QEHs_available.filter(d => d.qeh_type === type && (d.status === 1 || d.status === 2));
    return ret;
  }
}
