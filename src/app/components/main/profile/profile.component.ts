import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../services/api.service';
import { AuthenticationService } from '../../../services/authentication.service';
import { GET_AUTH_USER } from '../../../constants/urls';
import { User } from '../../../models/user.model';
import { plainToClass } from 'class-transformer';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  user: User;

  constructor(
    private auth: AuthenticationService,
    private http: ApiService,
  ) { }

  ngOnInit() {
    this.auth.authenticate();
    this.getUser();
  }

  getUser() {
    this.http.get_list(GET_AUTH_USER()).subscribe(
      res => {
        this.user = plainToClass(User, res['user'] as User);
      }
    );
  }
}
