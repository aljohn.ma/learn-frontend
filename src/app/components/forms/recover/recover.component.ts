import { Component, OnInit } from '@angular/core';
import { User } from '../../../models/user.model';
import { AuthenticationService } from '../../../services/authentication.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-recover',
  templateUrl: './recover.component.html',
  styleUrls: ['./recover.component.scss']
})
export class RecoverComponent implements OnInit {

  public form = new User();
  public is_saving = false;
  public button_text = 'Send password reset link';

  constructor(
    private http: AuthenticationService,
    private router: Router,
  ) { }

  ngOnInit() {

  }

  onSubmit() {
    this.changeStatus();
    this.http.recoverAccount(this.form)
      .subscribe(
        response => {
          this.changeStatus();
        },
        error => {
          this.changeStatus();
        }
      );
  }

  private changeStatus() {
    if (!this.is_saving) {
      this.is_saving = true;
      this.button_text = 'Sending email';
      return;
    }
    this.is_saving = false;
    this.button_text = 'Send password reset link';
  }
}
