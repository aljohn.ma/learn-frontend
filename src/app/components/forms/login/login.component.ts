import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../../services/authentication.service';
import { User } from '../../../models/user.model';
import { TokenService } from '../../../services/token.service';
import { LoginStatusService } from '../../../services/login.service';
import { Router } from '@angular/router';
import { NgFlashMessageService } from 'ng-flash-messages';
import { plainToClass } from 'class-transformer';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {


  public form = new User();
  loading = false;

  constructor(
    private authService: AuthenticationService,
    private router: Router,
    private token: TokenService,
    private login_status: LoginStatusService,
    private ngFlashMessageService: NgFlashMessageService


  ) { }

  ngOnInit() {
    if (this.token.get()) {
      this.router.navigate(['']);
    }
  }

  login() {
    this.loading =  true;
    const credits = { 'username': this.form.username, 'password': this.form.password };
    this.authService.loginUser(credits).
      subscribe(response => {

        this.token.handle(response);
        this.login_status.changeStatus(
          {
            status: true,
            access: response['access'],
            main_menu: response['access'].filter(x => x.dropdown_id === 0),
            setup_menu: response['access'].filter(x => x.dropdown_id === 1),
            user: plainToClass(User, response['user'] as User)
          }
        );
        this.loading = false;
        this.router.navigate(['']);
      },
        error => {
          const flashMessage = {
            messages: [error.error.error],
            timeout: 2000,
            type: 'error-1',
            dismissible: false
          };
          this.ngFlashMessageService.showFlashMessage(flashMessage);
          this.loading = false;
        }
      );
  }
}
