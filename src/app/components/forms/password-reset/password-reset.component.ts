import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../services/api.service';
import { RESET_PASSWORD } from '../../../constants/urls';
import { AuthenticationService } from '../../../services/authentication.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';


@Component({
  selector: 'app-password-reset',
  templateUrl: './password-reset.component.html',
  styleUrls: ['./password-reset.component.scss']
})
export class PasswordResetComponent implements OnInit {

  form = { reset_token: null }

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private http: AuthenticationService
  ) { 
  }

  ngOnInit() {
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.form.reset_token = params.get('token')
      console.log(this.form.reset_token);
    });
  }

  onSubmit(){
    console.log(this.form);
    
    this.http.changePassword(this.form)
        .subscribe(
          response=>{
            localStorage.removeItem('reset_token')
            console.log(response)
          },
          error =>{
            console.log(error)
          }
        );
  }

}
