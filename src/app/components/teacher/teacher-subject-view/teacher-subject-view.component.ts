import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from '../../../services/api.service';
import { GET_AUTH_TEACHER_SUBJECTS, API_QEH, PUBLISH_QEH } from '../../../constants/urls';
import { ActivatedRoute, Router } from '@angular/router';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { FLASH_LABEL } from '../../../constants/config';
import { NgFlashMessageService } from 'ng-flash-messages';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { QEH, QEH_STATUS, getQEHLength } from '../../../constants/constants';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-teacher-subject-view',
  templateUrl: './teacher-subject-view.component.html',
  styleUrls: ['./teacher-subject-view.component.scss']
})
export class TeacherSubjectViewComponent implements OnInit {

  @ViewChild('tblSubject') table_subject: DatatableComponent;

  QEH_status = QEH_STATUS;

  subject: any;
  QEHs: any;

  exams: any;
  quizzes: any;
  homeworks: any;
  exercises: any;

  obj: any = {};

  temp = [];
  search = '';
  loading = true;

  error: any = {};
  bsConfig: Partial<BsDatepickerConfig>;

  constructor(
    private http: ApiService,
    private route: ActivatedRoute,
    private flashMessage: NgFlashMessageService,
    private modalService: NgbModal,
    private datePipe: DatePipe,
    private router: Router
  ) { }

  ngOnInit() {
    this.bsConfig = Object.assign({}, { containerClass: 'theme-blue', dateInputFormat: 'MM/DD/YYYY' });

    this.fetch();
  }

  fetch() {
    this.loading = true;
    this.route.params.subscribe(
      par => {
        this.http.get(GET_AUTH_TEACHER_SUBJECTS(), par['id'])
          .subscribe(
            res => {
              this.subject = res['subject'];
              this.QEHs = this.subject.q_e_hs;
              this.temp = this.QEHs;
              this.categorizeQEH();
            },
            err => {
              if (err.status === 401) {
                this.router.navigate(['/401']);
              }
            }
          );
      });
    this.loading = false;
  }

  categorizeQEH() {
    this.quizzes = this.QEHs.filter(d => d.qeh_type === QEH.QUIZ);
    this.exams = this.QEHs.filter(d => d.qeh_type === QEH.EXAM);
    this.homeworks = this.QEHs.filter(d => d.qeh_type === QEH.HOMEWORK);
    this.exercises = this.QEHs.filter(d => d.qeh_type === QEH.EXERCISE);
  }

  getQEHLength(list: any, status: QEH_STATUS) {
    return getQEHLength(list, status);
  }

  updateFilter() {

    this.loading = true;
    const val = this.search.toLowerCase();

    const temp = this.temp.filter(function (d) {
      return d.qeh_code.toLowerCase().indexOf(val) !== -1 ||
        d.qeh_type_str.toLowerCase().indexOf(val) !== -1 ||
        d.status.toLowerCase().indexOf(val) !== -1 ||
        d.release_date.toLowerCase().indexOf(val) !== -1 ||
        d.end_date.toLowerCase().indexOf(val) !== -1 ||
        !val;
    });
    // update the rows
    this.QEHs = temp;
    // Whenever the filter changes, always go back to the first page
    this.table_subject.recalculate();
    this.table_subject.offset = 0;
    this.loading = false;
  }

  setQEH(row) {
    this.obj = row;
  }

  delete() {
    this.http.delete(API_QEH(), this.obj.id)
      .subscribe(
        res => {
          this.flash(res['success'], 'success');
          this.fetch();
        },
        err => {
          this.flash(err['failed'], 'danger');
        }
      );
  }

  flash(message, type) {
    const flashMessage = {
      messages: [message],
      timeout: 2000,
      type: FLASH_LABEL(type),
      dismissible: false
    };
    this.flashMessage.showFlashMessage(flashMessage);
  }

  publish() {
    this.obj.release_date = this.transform_date(this.obj.release_date);
    this.obj.end_date = this.transform_date(this.obj.end_date);
    this.http.put(PUBLISH_QEH(), this.obj.id, this.obj)
      .subscribe(
        res => {
          this.fetch();
          this.obj = null;
          this.modalService.dismissAll();
          this.error = {};
          this.flash(res['message'], 'success');
        },
        err => {
          this.handleError(err);
        }
      );
  }

  open(content, row) {
    this.obj = row;
    this.modalService.open(content, { centered: true, backdrop: 'static' });
  }

  dismiss() {
    this.fetch();
    this.modalService.dismissAll();
  }

  handleError(error) {
    this.error = error.error.errors;
    this.flash(error.error['message'], 'danger');
  }

  transform_date(date) {
    return this.datePipe.transform(date, 'MM/dd/yyyy');
  }
}
