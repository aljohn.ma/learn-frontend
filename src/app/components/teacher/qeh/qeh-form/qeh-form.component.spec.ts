import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QEHFormComponent } from './qeh-form.component';

describe('QEHFormComponent', () => {
  let component: QEHFormComponent;
  let fixture: ComponentFixture<QEHFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QEHFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QEHFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
