import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from '../../../../services/api.service';
import { GET_AUTH_TEACHER_SUBJECTS, API_QEH } from '../../../../constants/urls';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { FLASH_LABEL } from '../../../../constants/config';
import { NgFlashMessageService } from 'ng-flash-messages';
import { QEH_CATEGORY } from '../../../../constants/constants';

@Component({
  selector: 'app-qeh-form',
  templateUrl: './qeh-form.component.html',
  styleUrls: ['./qeh-form.component.scss']
})
export class QEHFormComponent implements OnInit {

  @Input() QEH: any = null;
  bsConfig: Partial<BsDatepickerConfig>;
  category = QEH_CATEGORY;

  subject: any = null;

  error: any = {};

  loading = false;


  constructor(
    private http: ApiService,
    private route: ActivatedRoute,
    private router: Router,
    private ngFlashMessageService: NgFlashMessageService

  ) { }

  ngOnInit() {
    this.bsConfig = Object.assign({}, { containerClass: 'theme-blue', dateInputFormat: 'MM/DD/YYYY' });
    this.getSubject(this.QEH.sl_id);
  }

  getSubject(id) {
    this.http.get(GET_AUTH_TEACHER_SUBJECTS(), id)
      .toPromise().then(
        res => {
          this.subject = res['subject'];
        },
        err => {
          if (err.status === 401) {
            this.router.navigate(['/401']);
          }
        }
      );
  }

  save() {
    this.loading = true;
    this.QEH.contents = this.hasContent();
    this.QEH.sh_id = this.subject.sh_id;
    if (this.QEH.id) {
      this.update();
    } else {
      this.create();
    }

  }

  create() {

    this.QEH.level_id = this.subject.head.section.level_id;
    this.QEH.section_id = this.subject.head.section_id;
    this.QEH.subject_id = this.subject.subject_id;

    this.http.post(API_QEH(), this.QEH).subscribe(
      res => {
        this.error = {};
        this.QEH.id = res['id'];
        this.next();
        this.loading = false;
      },
      err => {
        this.handleError(err);
      }
    );
  }

  update() {
    this.http.put(API_QEH(), this.QEH.id, this.QEH).subscribe(
      res => {
        this.error = {};
        this.next();
        this.loading = false;
      },
      err => {
        this.handleError(err);
      }
    );
  }

  next() {
    this.router.navigate([`/teacher/qeh/${this.QEH.id}/questions`]);
  }

  handleError(error) {
    this.error = error.error.errors;
    this.flash(error.error['message'], 'danger');
    this.loading = false;
  }

  flash(message, type) {
    const flashMessage = {
      messages: [message],
      timeout: 2000,
      type: FLASH_LABEL(type),
      dismissible: false
    };

    this.ngFlashMessageService.showFlashMessage(flashMessage);
  }

  cancel() {
    this.route.params.subscribe(
      par => {
        this.router.navigate([`/teacher/subjects/${this.QEH.sl_id}`]);
      });
  }

  hasContent() {
    return this.QEH.has_multiple_choice ||
      this.QEH.has_identification ||
      this.QEH.has_enumeration ||
      this.QEH.has_true_false ||
      this.QEH.has_essay ? true : null;
  }
}
