import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../../services/api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { API_QEH } from '../../../../constants/urls';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.scss']
})
export class ResultComponent implements OnInit {

  loading = true;
  id = 0;
  QEH: any;
  constructor(
    private http: ApiService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.route.params.subscribe(
      par => {
        this.id = par['id'];
        this.fetch();
      }
    );
  }

  fetch() {
    this.http.get_list(API_QEH(`/result/${this.id}`))
      .subscribe(
        res => {
          this.QEH = res['QEH'];
          this.loading = false;
        }
      );
  }
}
