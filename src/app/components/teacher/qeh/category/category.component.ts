import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from '../../../../services/api.service';
import { API_QEH } from '../../../../constants/urls';
import { HttpEventType } from '@angular/common/http';
import { getQEHCategory, QEH_CATEGORY } from '../../../../constants/constants';
import { DatatableComponent } from '@swimlane/ngx-datatable';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {

  @ViewChild('tblSubject') table_subject: DatatableComponent;
  temp = [];
  search = '';

  category = 0;
  QEHs: any;
  loading = true;

  constructor(
    private http: ApiService,
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit() {
    this.route.params.subscribe(
      par => {
        this.category = <QEH_CATEGORY>par['category'];
        this.fetch();
      }
    );
  }

  fetch() {
    this.http.get_list(
      API_QEH(`/category/${this.category}`), { reportProgress: true, observe: 'events' })
      .subscribe(
        event => {
          this.handleEvent(event);
        }
      );
  }

  handleEvent(event) {
    console.log(event);
    if (event.type === HttpEventType.Response) {
      this.QEHs = event.body.QEHs;
      this.temp = this.QEHs;
      this.loading = false;
    }
  }

  getCategoryStr() {
    return getQEHCategory(this.category, true);
  }

  updateFilter() {

    this.loading = true;
    const val = this.search.toLowerCase();

    const temp = this.temp.filter(function (d) {
      return d.qeh_code.toLowerCase().indexOf(val) !== -1 ||
        d.qeh_type_str.toLowerCase().indexOf(val) !== -1 ||
        d.status.toLowerCase().indexOf(val) !== -1 ||
        d.release_date.toLowerCase().indexOf(val) !== -1 ||
        d.end_date.toLowerCase().indexOf(val) !== -1 ||
        !val;
    });
    // update the rows
    this.QEHs = temp;
    // Whenever the filter changes, always go back to the first page
    this.table_subject.recalculate();
    this.table_subject.offset = 0;
    this.loading = false;
  }
}
