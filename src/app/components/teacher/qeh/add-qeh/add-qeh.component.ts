import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../../services/api.service';
import { GET_AUTH_TEACHER_SUBJECTS } from '../../../../constants/urls';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-add-qeh',
  templateUrl: './add-qeh.component.html',
  styleUrls: ['./add-qeh.component.scss']
})
export class AddQEHComponent implements OnInit {

  QEH: any = {};

  constructor(
    private http: ApiService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.route.params.subscribe(
      par => {
        this.QEH.qeh_type = null;
        this.QEH.sl_id = par['id'];
      }
    );
  }

}
