import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddQEHComponent } from './add-qeh.component';

describe('AddQEHComponent', () => {
  let component: AddQEHComponent;
  let fixture: ComponentFixture<AddQEHComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddQEHComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddQEHComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
