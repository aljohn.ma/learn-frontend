import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from '../../../../services/api.service';
import { API_QEH } from '../../../../constants/urls';

@Component({
  selector: 'app-edit-qeh',
  templateUrl: './edit-qeh.component.html',
  styleUrls: ['./edit-qeh.component.scss']
})
export class EditQEHComponent implements OnInit {

  QEH: any = {};

  constructor(
    private http: ApiService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.route.params.subscribe(
      par => {
        this.QEH.sl_id = par['id'];
        this.getQEH();
      }
    );
  }

  getQEH() {

    this.route.queryParams.subscribe(
      par => {
        this.http.get(API_QEH(), par['ref']).subscribe(
          res => {
            this.QEH = res['QEH'];
            this.QEH.release_date = new Date(this.QEH.release_date);
            this.QEH.end_date = new Date(this.QEH.end_date);
          },
          () => {
            this.router.navigate(['404']);
          }
        );
      }
    );

  }
}
