import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditQEHComponent } from './edit-qeh.component';

describe('EditQEHComponent', () => {
  let component: EditQEHComponent;
  let fixture: ComponentFixture<EditQEHComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditQEHComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditQEHComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
