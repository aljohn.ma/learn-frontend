import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TFFormComponent } from './tf-form.component';

describe('TFFormComponent', () => {
  let component: TFFormComponent;
  let fixture: ComponentFixture<TFFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TFFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TFFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
