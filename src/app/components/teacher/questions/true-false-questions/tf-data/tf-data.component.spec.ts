import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TFDataComponent } from './tf-data.component';

describe('TFDataComponent', () => {
  let component: TFDataComponent;
  let fixture: ComponentFixture<TFDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TFDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TFDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
