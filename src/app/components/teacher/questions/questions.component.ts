import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../services/api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { API_QEH } from '../../../constants/urls';

@Component({
  selector: 'app-questions',
  templateUrl: './questions.component.html',
  styleUrls: ['./questions.component.scss']
})
export class QuestionsComponent implements OnInit {

  QEH: any = {};

  constructor(
    private http: ApiService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.getQEH();
  }

  async getQEH() {
    await this.route.params.subscribe(
      par => {
        this.http.get(API_QEH(), par['id']).toPromise().then(
          res => {
            this.QEH = res['QEH'];
            if (!this.route.children.length) {
              this.redirect();
            }
          },
          err => {
            if (err.status === 401) {
              this.router.navigate(['/401']);
            }
          }
        );
      }
    );
  }
  redirect() {
    const rt = `teacher/qeh/${this.QEH.id}/questions/`;

    if (this.QEH.has_multiple_choice) {
      this.router.navigate([`${rt}multiple-choice`]);
      return;
    } else if (this.QEH.has_identification) {
      this.router.navigate([`${rt}identification`]);
      return;
    } else if (this.QEH.has_enumeration) {
      this.router.navigate([`${rt}enumeration`]);
      return;
    } else if (this.QEH.has_true_false) {
      this.router.navigate([`${rt}true-false`]);
      return;
    } else if (this.QEH.has_essay) {
      this.router.navigate([`${rt}essay`]);
      return;
    }

  }
}
