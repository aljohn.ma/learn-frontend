import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ENDataComponent } from './en-data.component';

describe('ENDataComponent', () => {
  let component: ENDataComponent;
  let fixture: ComponentFixture<ENDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ENDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ENDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
