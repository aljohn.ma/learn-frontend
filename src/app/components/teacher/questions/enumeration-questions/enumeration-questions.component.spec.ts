import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnumerationQuestionsComponent } from './enumeration-questions.component';

describe('EnumerationQuestionsComponent', () => {
  let component: EnumerationQuestionsComponent;
  let fixture: ComponentFixture<EnumerationQuestionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnumerationQuestionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnumerationQuestionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
