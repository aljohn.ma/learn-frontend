import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ENFormComponent } from './en-form.component';

describe('ENFormComponent', () => {
  let component: ENFormComponent;
  let fixture: ComponentFixture<ENFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ENFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ENFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
