import { Component, OnInit } from '@angular/core';
import { FLASH_LABEL } from '../../../../../constants/config';
import { API_QEH_EN } from '../../../../../constants/urls';
import { NgFlashMessageService } from 'ng-flash-messages';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from '../../../../../services/api.service';

@Component({
  selector: 'app-en-form',
  templateUrl: './en-form.component.html',
  styleUrls: ['./en-form.component.scss']
})
export class ENFormComponent implements OnInit {

  obj: any;
  error: any = {};

  constructor(
    private http: ApiService,
    private route: ActivatedRoute,
    private router: Router,
    private flashMessage: NgFlashMessageService,
  ) { }

  ngOnInit() {
    this.error.answers_arr = [];
    this.error.points_arr = [];
    this.route.params.subscribe(
      par => {
        if (par['id']) {
          this.fetch(par['id']);
        } else {
          this.newObj();
        }
      }
    );

  }

  fetch(id) {
    this.http.get(API_QEH_EN(), id).subscribe(
      res => {
        this.obj = res['question'];
        this.obj.removed_answers = [];
      }
    );
  }

  newObj() {
    this.obj = {};
    this.obj.answer = null;
    this.obj.answers = [];
    this.route.parent.parent.params
      .subscribe(
        par_1 => this.obj.qeh_id = par_1['id']
      );
  }

  addAnswer() {
    this.obj.answers.push({
      id: 0,
      en_id: this.obj.id,
      answer: null,
      points: 0
    });
  }

  removeAnswer(row, idx) {
    if (row.id) {
      this.obj.removed_answers.push(row.id);
    }
    this.obj.answers.splice(idx, 1);
  }

  save() {
    console.log(this.obj);

    if (this.obj.id) {
      this.update();
    } else {
      this.create();
    }
  }

  create() {
    this.http.post(API_QEH_EN(), this.obj).subscribe(
      res => {
        this.flash(res['success'], 'success');
        this.router.navigate(['../'], { relativeTo: this.route });
      },
      err => { this.handleError(err); }
    );
  }

  update() {
    this.http.put(API_QEH_EN(), this.obj.id, this.obj)
      .subscribe(
        res => {
          this.flash(res['success'], 'success');
          this.router.navigate(['../../'], { relativeTo: this.route });
        },
        err => {
          this.flash(err['failed'], 'danger');
        }
      );
  }

  handleError(error) {
    if (error.error.errors) {
      this.error = error.error.errors;
      if (this.obj.answers) {
        this.error.answers_arr = [];
        this.error.points_arr = [];
        for (let i = 0; i < this.obj.answers.length; i++) {
          const prop = `answers.${i}`;
          const ans = `${prop}.answer`;
          const point = `${prop}.points`;
          if (ans in this.error) {
            this.error.answers_arr[i] = this.error[ans];
            delete this.error[ans];
          }
          if (point in this.error) {
            this.error.points_arr[i] = this.error[point];
            delete this.error[point];
          }
        }
      }
    }
    this.flash(error.error['message'], 'danger');
  }

  flash(message, type) {
    const flashMessage = {
      messages: [message],
      timeout: 2000,
      type: FLASH_LABEL(type),
      dismissible: false
    };
    this.flashMessage.showFlashMessage(flashMessage);
  }

}
