import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../../../services/api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FLASH_LABEL } from '../../../../../constants/config';
import { API_QEH_MC } from '../../../../../constants/urls';
import { NgFlashMessageService } from 'ng-flash-messages';

@Component({
  selector: 'app-mc-form',
  templateUrl: './mc-form.component.html',
  styleUrls: ['./mc-form.component.scss']
})
export class McFormComponent implements OnInit {

  obj: any;
  error: any = {};

  constructor(
    private http: ApiService,
    private route: ActivatedRoute,
    private router: Router,
    private flashMessage: NgFlashMessageService,
  ) { }

  ngOnInit() {

    this.route.params.subscribe(
      par => {
        if (par['id']) {
          this.fetch(par['id']);
        } else {
          this.newObj();
        }
      }
    );

  }

  fetch(id) {
    this.http.get(API_QEH_MC(), id).subscribe(
      res => {
        this.obj = res['question'];
        console.log(res);
      }
    );
  }

  newObj() {
    this.obj = {};
    this.obj.answer = null;
    this.route.parent.parent.params
      .subscribe(
        par_1 => this.obj.qeh_id = par_1['id']
      );
  }

  save() {
    if (this.obj.id) {
      this.update();
    } else {
      this.create();
    }
  }

  create() {
    this.http.post(API_QEH_MC(), this.obj).subscribe(
      res => {
        this.flash(res['success'], 'success');
        this.router.navigate(['../'], { relativeTo: this.route });
      },
      err => { this.handleError(err); }
    );
  }

  update() {
    this.http.put(API_QEH_MC(), this.obj.id, this.obj)
      .subscribe(
        res => {
          this.flash(res['success'], 'success');
          this.router.navigate(['../../'], { relativeTo: this.route });
        },
        err => {
          this.flash(err['failed'], 'danger');
        }
      );
  }

  handleError(error) {
    if (error.error.errors) {
      this.error = error.error.errors;
    }
    this.flash(error.error['message'], 'danger');
  }

  flash(message, type) {
    const flashMessage = {
      messages: [message],
      timeout: 2000,
      type: FLASH_LABEL(type),
      dismissible: false
    };
    this.flashMessage.showFlashMessage(flashMessage);
  }

}
