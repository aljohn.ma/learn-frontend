import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { McFormComponent } from './mc-form.component';

describe('McFormComponent', () => {
  let component: McFormComponent;
  let fixture: ComponentFixture<McFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ McFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(McFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
