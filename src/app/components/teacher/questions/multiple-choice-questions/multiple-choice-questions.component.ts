import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../../services/api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { API_QEH_MC } from '../../../../constants/urls';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FLASH_LABEL } from '../../../../constants/config';
import { NgFlashMessageService } from 'ng-flash-messages';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';

@Component({
  selector: 'app-multiple-choice-questions',
  templateUrl: './multiple-choice-questions.component.html',
  styleUrls: ['./multiple-choice-questions.component.scss']
})
export class MultipleChoiceQuestionsComponent implements OnInit {

  constructor() { }

  ngOnInit() { }

}
