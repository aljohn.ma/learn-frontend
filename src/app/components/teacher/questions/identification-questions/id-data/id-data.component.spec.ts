import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IdDataComponent } from './id-data.component';

describe('IdDataComponent', () => {
  let component: IdDataComponent;
  let fixture: ComponentFixture<IdDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IdDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IdDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
