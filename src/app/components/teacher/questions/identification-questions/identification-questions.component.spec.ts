import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IdentificationQuestionsComponent } from './identification-questions.component';

describe('IdentificationQuestionsComponent', () => {
  let component: IdentificationQuestionsComponent;
  let fixture: ComponentFixture<IdentificationQuestionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IdentificationQuestionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IdentificationQuestionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
