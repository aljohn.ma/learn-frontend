import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ESSDataComponent } from './ess-data.component';

describe('ESSDataComponent', () => {
  let component: ESSDataComponent;
  let fixture: ComponentFixture<ESSDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ESSDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ESSDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
