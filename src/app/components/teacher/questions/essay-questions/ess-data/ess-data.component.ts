import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from '../../../../../services/api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NgFlashMessageService } from 'ng-flash-messages';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { FLASH_LABEL } from '../../../../../constants/config';
import { API_QEH_ESS } from '../../../../../constants/urls';
import { ExcelService } from '../../../../../services/excel.service';
import { HttpEventType } from '@angular/common/http';

@Component({
  selector: 'app-ess-data',
  templateUrl: './ess-data.component.html',
  styleUrls: ['./ess-data.component.scss']
})
export class ESSDataComponent implements OnInit {

  @ViewChild('tblQuestions') table: DatatableComponent;

  id = 0;
  questions: any;
  obj: any = {};
  temp: any;

  search = '';
  loading = true;
  error: any = {};

  upload_progress = 0;

  constructor(
    private http: ApiService,
    private route: ActivatedRoute,
    private router: Router,
    private flashMessage: NgFlashMessageService,
    private excel: ExcelService
  ) { }

  ngOnInit() {
    this.route.parent.parent.params.subscribe(
      par => {
        this.id = par['id'] as number;
        this.fetch();
      }
    );
  }

  fetch() {
    this.http.get(API_QEH_ESS('/get-questions'), this.id)
      .subscribe(
        res => {
          this.questions = res;
          this.temp = this.questions;
          this.loading = false;

        }
      );
  }

  updateFilter() {
    this.loading = true;
    const val = this.search.toLowerCase();

    const temp = this.temp.filter(function (d) {
      return d.question.toLowerCase().indexOf(val) !== -1 || !val;
    });

    // update the rows
    this.questions = temp;
    // Whenever the filter changes, always go back to the first page
    this.table.recalculate();
    this.table.offset = 0;
    this.loading = false;
  }

  setObj(row) {
    this.obj = row;
  }

  delete() {
    this.http.delete(API_QEH_ESS(), this.obj.id).subscribe(
      () => {
        this.flash('Delete question successful.', 'success');
      },
      () => {
        this.flash('Unable to delete question.', 'danger');
      }
    );
  }

  flash(message, type) {
    const flashMessage = {
      messages: [message],
      timeout: 2000,
      type: FLASH_LABEL(type),
      dismissible: false
    };
    this.flashMessage.showFlashMessage(flashMessage);
  }

  export() {

    this.excel.exportAsExcelFile(this.questions.map(({ qeh_id, answer, ...item }) => item), 'ess-questions', [
      { name: 'ID', width: 0 },
      { name: 'Question', width: 20 },
      { name: 'Points' }
    ]);

  }

  fileChangeEvent(evt: any) {
    this.upload_progress = 1;

    this.excel.importFromExcel(evt, ['id', 'question', 'points']).then(val => {

      const data = val;
      if (data) {
        this.http.post(API_QEH_ESS('/import-excel'),
          { questions: data, qeh_id: this.id },
          { reportProgress: true, observe: 'events' })
          .subscribe(
            event => {
              this.handleEvent(event);
            }
          );
      }
      this.upload_progress = 0;
    }).catch(err => {
      this.upload_progress = 0;
    });

  }

  handleEvent(event) {
    if (event.type === HttpEventType.Response) {
      if (event.status === 200) {
        this.flash(event.body.success, 'success');
        this.fetch();
      } else if (event.status === 422) {
        this.flash(event.body.failed, 'danger');
      } else {
        this.handleError(event);
      }
    }
  }

  handleError(err) {
    this.flash(err.body.message, 'danger');
  }
}
