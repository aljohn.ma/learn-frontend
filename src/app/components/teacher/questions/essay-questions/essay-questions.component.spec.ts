import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EssayQuestionsComponent } from './essay-questions.component';

describe('EssayQuestionsComponent', () => {
  let component: EssayQuestionsComponent;
  let fixture: ComponentFixture<EssayQuestionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EssayQuestionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EssayQuestionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
