import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ESSFormComponent } from './ess-form.component';

describe('ESSFormComponent', () => {
  let component: ESSFormComponent;
  let fixture: ComponentFixture<ESSFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ESSFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ESSFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
