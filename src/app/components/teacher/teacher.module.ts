import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import { TeacherRoutingModule, TeacherRoutingComponents } from './teacher-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxDatatableWatcherDirective } from '../../directives/ngx-datatable-watcher.directive';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { QuestionsComponent } from './questions/questions.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MultipleChoiceQuestionsComponent } from './questions/multiple-choice-questions/multiple-choice-questions.component';
import { IdentificationQuestionsComponent } from './questions/identification-questions/identification-questions.component';
import { EnumerationQuestionsComponent } from './questions/enumeration-questions/enumeration-questions.component';
import { TrueFalseQuestionsComponent } from './questions/true-false-questions/true-false-questions.component';
import { EssayQuestionsComponent } from './questions/essay-questions/essay-questions.component';
import { TruncatePipe } from '../../pipes/truncate.pipe';
import { SharedModule } from '../../shared.module';
import { ResultComponent } from './qeh/result/result.component';

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    TeacherRoutingModule,
    NgxDatatableModule,
    BsDatepickerModule.forRoot(),
    NgxMaterialTimepickerModule.forRoot(),
    NgbModule.forRoot(),
    SharedModule
  ],

  declarations: [
    NgxDatatableWatcherDirective,
    TeacherRoutingComponents,
    QuestionsComponent,
    MultipleChoiceQuestionsComponent,
    IdentificationQuestionsComponent,
    EnumerationQuestionsComponent,
    TrueFalseQuestionsComponent,
    EssayQuestionsComponent,
    TruncatePipe,

  ],
  providers: [DatePipe]
})
export class TeacherModule { }
