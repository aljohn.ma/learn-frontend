import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from '../../../services/api.service';
import { GET_AUTH_TEACHER_SUBJECTS } from '../../../constants/urls';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { FLASH_LABEL } from '../../../constants/config';
import { NgFlashMessageService } from 'ng-flash-messages';
import { Router } from '@angular/router';

@Component({
  selector: 'app-teacher-subjects',
  templateUrl: './teacher-subjects.component.html',
  styleUrls: ['./teacher-subjects.component.scss']
})
export class TeacherSubjectsComponent implements OnInit {

  subjects: any = [];
  loading = true;
  search = '';
  temp = [];

  @ViewChild('tblSubject') table_subject: DatatableComponent;

  constructor(
    private http: ApiService,
    private flashMessage: NgFlashMessageService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.fetch();
  }

  fetch() {
    this.http.get_list(GET_AUTH_TEACHER_SUBJECTS())
      .subscribe(
        res => {
          this.subjects = res['subjects'];
          this.temp = [...this.subjects];
          this.loading = false;
        },
        err => {
          if (err.status === 401) {
            this.router.navigate(['/401']);
          }
        }
      );

  }

  updateFilter() {

    this.loading = true;
    const val = this.search.toLowerCase();

    const temp = this.temp.filter(function (d) {
      return d.subject.subject_code.toLowerCase().indexOf(val) !== -1 || d.room.room_name.toLowerCase().indexOf(val) !== -1 || !val;
    });
    // update the rows
    this.subjects = temp;
    // Whenever the filter changes, always go back to the first page
    this.table_subject.recalculate();
    this.table_subject.offset = 0;
    this.loading = false;
  }

  flash(message, type) {
    const flashMessage = {
      messages: [message],
      timeout: 30000,
      type: FLASH_LABEL(type),
      dismissible: false
    };
    this.flashMessage.showFlashMessage(flashMessage);
  }
}
