import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TeacherSubjectsComponent } from './teacher-subjects/teacher-subjects.component';
import { TeacherSubjectViewComponent } from './teacher-subject-view/teacher-subject-view.component';
import { AddQEHComponent } from './qeh/add-qeh/add-qeh.component';
import { EditQEHComponent } from './qeh/edit-qeh/edit-qeh.component';
import { QEHFormComponent } from './qeh/qeh-form/qeh-form.component';
import { QuestionsComponent } from './questions/questions.component';
import { IdentificationQuestionsComponent } from './questions/identification-questions/identification-questions.component';
import { EnumerationQuestionsComponent } from './questions/enumeration-questions/enumeration-questions.component';
import { TrueFalseQuestionsComponent } from './questions/true-false-questions/true-false-questions.component';
import { EssayQuestionsComponent } from './questions/essay-questions/essay-questions.component';
import { MultipleChoiceQuestionsComponent } from './questions/multiple-choice-questions/multiple-choice-questions.component';
import { McFormComponent } from './questions/multiple-choice-questions/mc-form/mc-form.component';
import { McDataComponent } from './questions/multiple-choice-questions/mc-data/mc-data.component';
import { IdDataComponent } from './questions/identification-questions/id-data/id-data.component';
import { IdFormComponent } from './questions/identification-questions/id-form/id-form.component';
import { TFFormComponent } from './questions/true-false-questions/tf-form/tf-form.component';
import { TFDataComponent } from './questions/true-false-questions/tf-data/tf-data.component';
import { ESSDataComponent } from './questions/essay-questions/ess-data/ess-data.component';
import { ESSFormComponent } from './questions/essay-questions/ess-form/ess-form.component';
import { ENFormComponent } from './questions/enumeration-questions/en-form/en-form.component';
import { ENDataComponent } from './questions/enumeration-questions/en-data/en-data.component';
import { CategoryComponent } from './qeh/category/category.component';
import { ResultComponent } from './qeh/result/result.component';

const routes: Routes = [
  { path: 'subjects', component: TeacherSubjectsComponent },
  { path: 'subjects/:id', component: TeacherSubjectViewComponent },
  { path: 'qeh/category/:category', component: CategoryComponent },
  { path: 'qeh/add/:id', component: AddQEHComponent },
  { path: 'qeh/edit/:id', component: EditQEHComponent },
  { path: 'qeh/:id/result', component: ResultComponent },
  {
    path: 'qeh/:id/questions',
    component: QuestionsComponent,
    children: [
      {
        path: 'multiple-choice',
        component: MultipleChoiceQuestionsComponent,
        children: [
          { path: '', pathMatch: 'full', component: McDataComponent },
          { path: 'add', component: McFormComponent },
          { path: 'edit/:id', component: McFormComponent }
        ]
      },
      {
        path: 'identification',
        component: IdentificationQuestionsComponent,
        children: [
          { path: '', component: IdDataComponent },
          { path: 'add', component: IdFormComponent },
          { path: 'edit/:id', component: IdFormComponent }
        ]
      },
      {
        path: 'enumeration',
        component: EnumerationQuestionsComponent,
        children: [
          { path: '', component: ENDataComponent },
          { path: 'add', component: ENFormComponent },
          { path: 'edit/:id', component: ENFormComponent }
        ]
      },
      {
        path: 'true-false',
        component: TrueFalseQuestionsComponent,
        children: [
          { path: '', component: TFDataComponent },
          { path: 'add', component: TFFormComponent },
          { path: 'edit/:id', component: TFFormComponent }
        ]
      },
      {
        path: 'essay',
        component: EssayQuestionsComponent,
        children: [
          { path: '', component: ESSDataComponent },
          { path: 'add', component: ESSFormComponent },
          { path: 'edit/:id', component: ESSFormComponent }
        ]
      },
    ]
  },



];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TeacherRoutingModule { }

export const TeacherRoutingComponents = [
  TeacherSubjectsComponent,
  TeacherSubjectViewComponent,
  AddQEHComponent,
  EditQEHComponent,
  QEHFormComponent,
  MultipleChoiceQuestionsComponent,
  IdentificationQuestionsComponent,
  EnumerationQuestionsComponent,
  TrueFalseQuestionsComponent,
  EssayQuestionsComponent,
  McFormComponent,
  McDataComponent,
  IdDataComponent,
  IdFormComponent,
  TFFormComponent,
  TFDataComponent,
  ESSDataComponent,
  ESSFormComponent,
  ENFormComponent,
  ENDataComponent,
  CategoryComponent,
  ResultComponent,

];

