import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';

// INSTALLED MODULES
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DataTablesModule } from 'angular-datatables';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { DatepickerModule, BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { NgFlashMessagesModule } from 'ng-flash-messages';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { SetupRoutingModule, SetupRoutingComponents } from './setup-routing.module';
import { ImageCropperModule } from 'ngx-image-cropper';
import { SharedModule } from '../../shared.module';

@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    SetupRoutingModule,
    NgxDatatableModule,
    BsDatepickerModule.forRoot(),
    DatepickerModule.forRoot(),
    NgbModule,
    NgxMaterialTimepickerModule.forRoot(),
    ImageCropperModule,
    SharedModule
  ],
  declarations: [
    SetupRoutingComponents
  ]
})
export class SetupModule { }
