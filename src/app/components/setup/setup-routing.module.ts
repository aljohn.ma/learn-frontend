import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LevelsComponent } from './levels/levels.component';

import { GrantPageAccessService } from '../../services/grant-access.service';
import { SubjectsComponent } from './subjects/subjects.component';
import { RoomsComponent } from './rooms/rooms.component';
import { SectionsComponent } from './sections/sections.component';
import { ScheduleComponent } from '../schedule/schedule.component';
import { ScheduleLinesComponent } from '../schedule/schedule-lines/schedule-lines.component';
import { ClassPositionsComponent } from './class-positions/class-positions.component';

const routes: Routes = [
  { path: 'levels', component: LevelsComponent, canActivate: [GrantPageAccessService] },
  { path: 'subjects', component: SubjectsComponent, canActivate: [GrantPageAccessService] },
  { path: 'rooms', component: RoomsComponent, canActivate: [GrantPageAccessService] },
  { path: 'sections', component: SectionsComponent, canActivate: [GrantPageAccessService] },
  { path: 'schedules', component: ScheduleComponent, canActivate: [GrantPageAccessService] },
  { path: 'schedules/:id', component: ScheduleLinesComponent },
  { path: 'class-positions', component: ClassPositionsComponent, canActivate: [GrantPageAccessService] },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SetupRoutingModule { }

export const SetupRoutingComponents = [
  LevelsComponent,
  SubjectsComponent,
  RoomsComponent,
  SectionsComponent,
  ScheduleComponent,
  ScheduleLinesComponent,
  ClassPositionsComponent,
];
