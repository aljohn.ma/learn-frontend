import { Component, OnInit, ViewChild } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { NgbPopover } from '@ng-bootstrap/ng-bootstrap';
import { Access } from '../../../models/access.model';
import { ApiService } from '../../../services/api.service';
import { AuthenticationService } from '../../../services/authentication.service';
import { NgFlashMessageService } from 'ng-flash-messages';
import { plainToClass } from 'class-transformer';
import { API_SUBJECTS } from '../../../constants/urls';
import { FLASH_LABEL } from '../../../constants/config';

@Component({
  selector: 'app-subjects',
  templateUrl: './subjects.component.html',
  styleUrls: ['./subjects.component.scss']
})
export class SubjectsComponent implements OnInit {

  @ViewChild(DatatableComponent) table: DatatableComponent;

  temp = [];
  subjects: any = [];
  loading = true;
  access: Access = null;
  obj: any = {};
  search = '';

  constructor(
    private http: ApiService,
    private auth: AuthenticationService,
    private ngFlashMessageService: NgFlashMessageService

  ) { }

  async ngOnInit() {
    await this.auth.getAccess('subjects').toPromise().then(
      res => {
        this.access = plainToClass(Access, res['access'] as Access);
      }
    );

    if (this.access) {
      this.fetch();
    }
  }

  fetch() {
    this.http.get_list(API_SUBJECTS())
      .subscribe(
        res => {
          this.subjects = res;
          this.temp = [...this.subjects];
          this.loading = false;
        },
      );
  }

  updateFilter() {

    const val = this.search.toLowerCase();

    const temp = this.temp.filter(function (d) {
      return d.subject_code.toLowerCase().indexOf(val) !== -1 || d.subject_name.toLowerCase().indexOf(val) !== -1 || !val;
    });

    // update the rows
    this.subjects = temp;
    // Whenever the filter changes, always go back to the first page
    this.table.recalculate();
    this.table.offset = 0;
  }

  add() {
    this.obj = {};
  }

  edit(obj) {
    this.obj = obj;
  }

  delete(id) {
    this.loading = true;
    this.http.delete(API_SUBJECTS(), id).subscribe(
      () => {
        this.fetch();
        this.flash('Delete subject successful.', 'success');
      }
    );
  }

  save() {
    this.loading = true;
    if (this.not_existing(this.obj)) {
      if (this.obj.id) {
        this.http.put(API_SUBJECTS(), this.obj.id, this.obj).subscribe(
          () => {
            this.fetch();
            this.obj = {};
            this.flash('Update subject successful.', 'success');
          },
          err => {
            console.log(err);
            this.loading = false;
          }
        );
        return;
      }
      this.http.post(API_SUBJECTS(), this.obj).subscribe(
        () => {
          this.fetch();
          this.obj = {};
          this.flash('Subject created successfully.', 'success');
        },
        err => {
          console.log(err);
          this.loading = false;

        }
      );
    } else {
      this.flash('Duplicate subject detected.', 'error');
      this.loading = false;
    }
  }

  cancel() {
    this.obj = {};
  }

  private not_existing(obj) {
    let existing = this.subjects.find(x => x.subject_code.toLowerCase() === obj.subject_code.toLowerCase());
    if (obj.id) {
      existing = this.subjects.find(x => x.subject_code.toLowerCase() === obj.subject_code.toLowerCase() && x.id !== obj.id);
    }
    return !existing;
  }

  flash(message, type) {
    const flashMessage = {
      messages: [message],
      timeout: 2000,
      type: FLASH_LABEL(type),
      dismissible: false
    };

    this.ngFlashMessageService.showFlashMessage(flashMessage);
  }

}
