import { Component, OnInit, ViewChild } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { NgbPopover } from '@ng-bootstrap/ng-bootstrap';
import { Access } from '../../../models/access.model';
import { AuthenticationService } from '../../../services/authentication.service';
import { ApiService } from '../../../services/api.service';
import { NgFlashMessageService } from 'ng-flash-messages';
import { plainToClass } from 'class-transformer';
import { API_SECTIONS } from '../../../constants/urls';
import { FLASH_LABEL } from '../../../constants/config';

@Component({
  selector: 'app-sections',
  templateUrl: './sections.component.html',
  styleUrls: ['./sections.component.scss']
})
export class SectionsComponent implements OnInit {

  @ViewChild(DatatableComponent) table: DatatableComponent;

  temp = [];
  sections: any = [];
  loading = true;
  access: Access = null;

  error: any = {};
  obj: any = {};

  year_levels: any = [];

  search = '';

  constructor(
    private http: ApiService,
    private auth: AuthenticationService,
    private ngFlashMessageService: NgFlashMessageService

  ) { }

  async ngOnInit() {

    await this.auth.getAccess('sections').toPromise().then(
      res => {
        this.access = plainToClass(Access, res['access'] as Access);
      }
    );

    if (this.access) {
      this.fetch();

    }
  }

  fetch() {
    this.http.get_list(API_SECTIONS())
      .subscribe(
        res => {
          this.sections = res['sections'];
          this.temp = [...this.sections];
          this.year_levels = res['levels'];
          this.loading = false;
          this.new_obj();
        },
      );
  }

  updateFilter() {
    this.loading = true;
    const val = this.search.toLowerCase();

    const temp = this.temp.filter(function (d) {
      return d.section_code.toLowerCase().indexOf(val) !== -1 || d.section_name.toLowerCase().indexOf(val) !== -1 || !val;
    });

    // update the rows
    this.sections = temp;
    // Whenever the filter changes, always go back to the first page
    this.table.recalculate();
    this.table.offset = 0;
    this.loading = false;
  }

  add() {
    this.obj = { leve_id: null };
  }

  edit(obj) {
    this.obj = obj;
  }

  delete(id) {
    this.loading = true;
    this.http.delete(API_SECTIONS(), id).subscribe(
      () => {
        this.fetch();
        this.flash('Delete section successful.', 'success');
      }
    );
  }

  save() {
    this.loading = true;
    if (this.not_existing(this.obj)) {
      if (this.obj.id) {
        this.http.put(API_SECTIONS(), this.obj.id, this.obj).subscribe(
          () => {
            this.fetch();
            this.flash('Update section successful.', 'success');
          },
          err => {
            this.handleError(err);
            this.loading = false;
          }
        );
        return;
      }
      this.http.post(API_SECTIONS(), this.obj).subscribe(
        () => {
          this.fetch();
          this.flash('Section created successfully.', 'success');
        },
        err => {
          this.handleError(err);
          this.loading = false;

        }
      );
    } else {
      this.flash('Duplicate section detected.', 'error');
      this.loading = false;
    }
  }
  handleError(error) {
    this.error = error.error.errors;
    this.flash(error.error.message, 'danger');
  }
  cancel() {
    this.new_obj();
  }

  new_obj() {
    this.obj = { level_id: null };
    this.error = {};
  }

  private not_existing(obj) {

    let existing = this.sections.find(x => x.section_code.toLowerCase() === obj.section_code.toLowerCase());
    if (obj.id) {
      existing = this.sections.find(x => x.section_code.toLowerCase() === obj.section_code.toLowerCase() && x.id !== obj.id);
    }

    return !existing;
  }

  flash(message, type) {
    const flashMessage = {
      messages: [message],
      timeout: 2000,
      type:  FLASH_LABEL(type),
      dismissible: false
    };

    this.ngFlashMessageService.showFlashMessage(flashMessage);
  }

}
