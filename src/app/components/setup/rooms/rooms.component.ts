import { Component, OnInit, ViewChild } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { NgbPopover } from '@ng-bootstrap/ng-bootstrap';
import { Access } from '../../../models/access.model';
import { ApiService } from '../../../services/api.service';
import { AuthenticationService } from '../../../services/authentication.service';
import { NgFlashMessageService } from 'ng-flash-messages';
import { plainToClass } from 'class-transformer';
import { API_ROOMS } from '../../../constants/urls';
import { FLASH_LABEL } from '../../../constants/config';

@Component({
  selector: 'app-rooms',
  templateUrl: './rooms.component.html',
  styleUrls: ['./rooms.component.scss']
})
export class RoomsComponent implements OnInit {

  @ViewChild(DatatableComponent) table: DatatableComponent;

  temp = [];
  rooms: any = [];
  loading = true;
  access: Access = null;
  obj: any = {};
  search = '';

  constructor(
    private http: ApiService,
    private auth: AuthenticationService,
    private ngFlashMessageService: NgFlashMessageService

  ) { }

  async ngOnInit() {
    await this.auth.getAccess('rooms').toPromise().then(
      res => {
        this.access = plainToClass(Access, res['access'] as Access);
      }
    );

    if (this.access) {
      this.fetch();
    }
  }

  fetch() {
    this.http.get_list(API_ROOMS())
      .subscribe(
        res => {
          this.rooms = res;
          this.temp = [...this.rooms];
          this.loading = false;
        },
      );
  }

  updateFilter() {
    this.loading = true;
    const val = this.search.toLowerCase();

    const temp = this.temp.filter(function (d) {
      return d.room_code.toLowerCase().indexOf(val) !== -1 || d.room_name.toLowerCase().indexOf(val) !== -1 || !val;
    });

    // update the rows
    this.rooms = temp;
    // Whenever the filter changes, always go back to the first page
    this.table.recalculate();
    this.table.offset = 0;
    this.loading = false;
  }

  add() {
    this.obj = {};
  }

  edit(obj) {
    this.obj = obj;
  }

  delete(id) {
    this.loading = true;
    this.http.delete(API_ROOMS(), id).subscribe(
      () => {
        this.fetch();
        this.flash('Delete room successful.', 'success');
      }
    );
  }

  save() {
    this.loading = true;
    if (this.not_existing(this.obj)) {
      if (this.obj.id) {
        this.http.put(API_ROOMS(), this.obj.id, this.obj).subscribe(
          () => {
            this.fetch();
            this.obj = {};
            this.flash('Update room successful.', 'success');
          },
          err => {
            console.log(err);
            this.loading = false;
          }
        );
        return;
      }
      this.http.post(API_ROOMS(), this.obj).subscribe(
        () => {
          this.fetch();
          this.obj = {};
          this.flash('Room created successfully.', 'success');
        },
        err => {
          console.log(err);
          this.loading = false;

        }
      );
    } else {
      this.flash('Duplicate room detected.', 'error');
      this.loading = false;
    }
  }

  cancel() {
    this.obj = {};
  }

  private not_existing(obj) {
    let existing = this.rooms.find(x => x.room_code.toLowerCase() === obj.room_code.toLowerCase());
    if (obj.id) {
      existing = this.rooms.find(x => x.room_code.toLowerCase() === obj.room_code.toLowerCase() && x.id !== obj.id);
    }
    return !existing;
  }

  flash(message, type) {
    const flashMessage = {
      messages: [message],
      timeout: 2000,
      type: FLASH_LABEL(type),
      dismissible: false
    };

    this.ngFlashMessageService.showFlashMessage(flashMessage);
  }

}
