import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClassPositionsComponent } from './class-positions.component';

describe('ClassPositionsComponent', () => {
  let component: ClassPositionsComponent;
  let fixture: ComponentFixture<ClassPositionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClassPositionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassPositionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
