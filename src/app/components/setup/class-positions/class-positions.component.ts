import { Component, OnInit, ViewChild } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Access } from '../../../models/access.model';
import { AuthenticationService } from '../../../services/authentication.service';
import { NgFlashMessageService } from 'ng-flash-messages';
import { ApiService } from '../../../services/api.service';
import { plainToClass } from 'class-transformer';
import { API_CLASS_POSITIONS } from '../../../constants/urls';
import { FLASH_LABEL } from '../../../constants/config';

@Component({
  selector: 'app-class-positions',
  templateUrl: './class-positions.component.html',
  styleUrls: ['./class-positions.component.scss']
})
export class ClassPositionsComponent implements OnInit {

  @ViewChild(DatatableComponent) table: DatatableComponent;

  temp = [];
  list: any = [];
  loading = true;
  access: Access = null;
  obj: any = {};
  search = '';

  constructor(
    private http: ApiService,
    private auth: AuthenticationService,
    private ngFlashMessageService: NgFlashMessageService

  ) { }

  async ngOnInit() {
    await this.auth.getAccess('class-positions').toPromise().then(
      res => {
        this.access = plainToClass(Access, res['access'] as Access);
      }
    );

    if (this.access) {
      this.fetch();
    }
  }

  fetch() {
    this.http.get_list(API_CLASS_POSITIONS())
      .subscribe(
        res => {
          this.list = res;
          this.temp = [...this.list];
          this.loading = false;
        },
      );
  }

  updateFilter() {

    this.loading = true;
    const val = this.search.toLowerCase();

    const temp = this.temp.filter(function (d) {
      return d.class_position_name.toLowerCase().indexOf(val) !== -1 || d.class_position_code.toLowerCase().indexOf(val) !== -1 || !val;
    });

    // update the rows
    this.list = temp;
    // Whenever the filter changes, always go back to the first page
    this.table.recalculate();
    this.table.offset = 0;
    this.loading = false;
  }

  add() {
    this.obj = {};
  }

  edit(obj) {
    this.obj = obj;
  }

  delete(id) {
    this.loading = true;
    this.http.delete(API_CLASS_POSITIONS(), id).subscribe(
      () => {
        this.fetch();
        this.flash('Delete class position successful.', 'success');
      }
    );
  }

  save() {
    this.loading = true;
    if (this.not_existing(this.obj)) {
      if (this.obj.id) {
        this.http.put(API_CLASS_POSITIONS(), this.obj.id, this.obj).subscribe(
          () => {
            this.fetch();
            this.obj = {};
            this.flash('Update class position successful.', 'success');
          },
          err => {
            console.log(err);
            this.loading = false;
          }
        );
        return;
      }
      this.http.post(API_CLASS_POSITIONS(), this.obj).subscribe(
        () => {
          this.fetch();
          this.obj = {};
          this.flash('Class position created successfully.', 'success');
        },
        err => {
          console.log(err);
          this.loading = false;

        }
      );
    } else {
      this.flash('Duplicate class position detected.', 'error');
      this.loading = false;
    }
  }

  cancel() {
    this.obj = {};
  }

  private not_existing(obj) {
    let existing = this.list.find(x => x.class_position_name.toLowerCase() === obj.class_position_name.toLowerCase());
    if (obj.id) {
      existing = this.list.find(x => x.class_position_name.toLowerCase() === obj.class_position_name.toLowerCase() && x.id !== obj.id);
    }
    return !existing;
  }

  flash(message, type) {
    const flashMessage = {
      messages: [message],
      timeout: 2000,
      type: FLASH_LABEL(type),
      dismissible: false
    };

    this.ngFlashMessageService.showFlashMessage(flashMessage);
  }

}
