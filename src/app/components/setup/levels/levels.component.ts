import { Component, OnInit, ViewChild } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { ApiService } from '../../../services/api.service';
import { Access } from '../../../models/access.model';
import { plainToClass } from 'class-transformer';
import { AuthenticationService } from '../../../services/authentication.service';
import { API_LEVELS } from '../../../constants/urls';
import { NgbPopover } from '@ng-bootstrap/ng-bootstrap';
import { NgFlashMessageService } from 'ng-flash-messages';
import { FLASH_LABEL } from '../../../constants/config';


@Component({
  selector: 'app-levels',
  templateUrl: './levels.component.html',
  styleUrls: ['./levels.component.scss']
})
export class LevelsComponent implements OnInit {

  @ViewChild(DatatableComponent) table: DatatableComponent;
  @ViewChild('levelPop') public levelPop: NgbPopover;

  temp = [];
  levels: any = [];
  loading = true;
  access: Access = null;
  obj: any = null;
  search = '';

  constructor(
    private http: ApiService,
    private auth: AuthenticationService,
    private ngFlashMessageService: NgFlashMessageService

  ) { }

  async ngOnInit() {
    await this.auth.getAccess('levels').toPromise().then(
      res => {
        this.access = plainToClass(Access, res['access'] as Access);
      }
    );

    if (this.access) {
      this.fetch();
    }
  }

  fetch() {
    this.http.get_list(API_LEVELS())
      .subscribe(
        res => {
          this.levels = res;
          this.temp = [...this.levels];
          this.loading = false;
        },
      );
  }

  updateFilter() {
    this.loading = true;
    const val = this.search.toLowerCase();

    const temp = this.temp.filter(function (d) {
      return d.level_name.toLowerCase().indexOf(val) !== -1 || !val;
    });

    // update the rows
    this.levels = temp;
    // Whenever the filter changes, always go back to the first page
    this.table.recalculate();
    this.table.offset = 0;
    this.loading = false;

  }

  add() {
    this.obj = {};
  }

  edit(obj) {
    this.obj = obj;
  }

  delete(id) {
    this.loading = true;
    this.http.delete(API_LEVELS(), id).subscribe(
      () => {
        this.fetch();
        this.flash('Delete level successful.', 'success');
      }
    );
  }

  save() {
    this.loading = true;
    if (this.not_existing(this.obj)) {
      if (this.obj.id) {
        this.http.put(API_LEVELS(), this.obj.id, this.obj).subscribe(
          () => {
            this.fetch();
            this.obj = null;
            this.flash('Update level successful.', 'success');
          },
          err => {
            console.log(err);
            this.loading = false;
          }
        );
        return;
      }
      this.http.post(API_LEVELS(), this.obj).subscribe(
        () => {
          this.fetch();
          this.obj = {};
          this.flash('Level created successfully.', 'success');
        },
        err => {
          console.log(err);
          this.loading = false;

        }
      );
    }
  }
  cancel() {
    this.obj = null;
  }

  private not_existing(obj) {
    let existing = this.levels.find(x => x.level_name.toLowerCase() === obj.level_name.toLowerCase());
    if (obj.id) {
      existing = this.levels.find(x => x.level_name.toLowerCase() === obj.level_name.toLowerCase() && x.id !== obj.id);
    }
    return !existing;
  }

  flash(message, type) {
    const flashMessage = {
      messages: [message],
      timeout: 2000,
      type: FLASH_LABEL(type),
      dismissible: false
    };

    this.ngFlashMessageService.showFlashMessage(flashMessage);
  }
}
