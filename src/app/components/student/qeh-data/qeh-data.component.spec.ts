import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QEHDataComponent } from './qeh-data.component';

describe('QEHDataComponent', () => {
  let component: QEHDataComponent;
  let fixture: ComponentFixture<QEHDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QEHDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QEHDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
