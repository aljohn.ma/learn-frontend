import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QEHComponent } from './qeh.component';

describe('QEHComponent', () => {
  let component: QEHComponent;
  let fixture: ComponentFixture<QEHComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QEHComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QEHComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
