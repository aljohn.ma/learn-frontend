import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../services/api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { GET_STUDENT_QEH } from '../../../constants/urls';
import { NgFlashMessageService } from 'ng-flash-messages';
import { FLASH_LABEL } from '../../../constants/config';

@Component({
  selector: 'app-qeh',
  templateUrl: './qeh.component.html',
  styleUrls: ['./qeh.component.scss']
})
export class QEHComponent implements OnInit {

  STUDENT_QEH: any;

  enum_length = 0;
  id_length = 0;
  mc_length = 0;
  tf_length = 0;
  ess_length = 0;

  constructor(
    private http: ApiService,
    private route: ActivatedRoute,
    private router: Router,
    private flashMessage: NgFlashMessageService) { }

  ngOnInit() {
    this.fetch();
  }

  fetch() {
    this.route.params.subscribe(
      par => {
        const id = par['id'];
        this.http.get(GET_STUDENT_QEH(), id).subscribe(
          res => {
            this.STUDENT_QEH = res['student_qeh'];
            if (this.STUDENT_QEH.status === 3) {
              this.router.navigate(['./result'], { relativeTo: this.route });
            } else if (this.STUDENT_QEH.status === 1 && this.STUDENT_QEH.status_str !== 'Ready') {
              this.flash('QEH not ready.', 'warning');
              this.router.navigate(['/']);
            } else if (this.STUDENT_QEH.status === 2) {
              this.router.navigate(['./take'], { relativeTo: this.route });
            } else {
              this.enum_length = res['enum_length'];
              this.id_length = res['id_length'];
              this.mc_length = res['mc_length'];
              this.tf_length = res['tf_length'];
              this.ess_length = res['ess_length'];
            }
          }
        );
      }
    );
  }

  flash(message, type) {
    const flashMessage = {
      messages: [message],
      timeout: 2000,
      type: FLASH_LABEL(type),
      dismissible: false
    };
    this.flashMessage.showFlashMessage(flashMessage);
  }

  getQEHContents() {
    const ret = [];
    if (this.STUDENT_QEH.q_e_h.has_multiple_choice) {
      ret.push({ name: 'Multiple choice', class: 'label-danger' });
    } else if (this.STUDENT_QEH.q_e_h.has_identification) {
      ret.push({ name: 'Identification', class: 'label-info' });
    } else if (this.STUDENT_QEH.q_e_h.has_enumeration) {
      ret.push({ name: 'Enumeration', class: 'label-warning' });
    } else if (this.STUDENT_QEH.q_e_h.has_true_false) {
      ret.push({ name: 'True or False', class: 'label-success' });
    } else if (this.STUDENT_QEH.q_e_h.has_essay) {
      ret.push({ name: 'Essay', class: 'label-primary' });
    }
    return ret;
  }
}
