import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentHomeworksComponent } from './student-homeworks.component';

describe('StudentHomeworksComponent', () => {
  let component: StudentHomeworksComponent;
  let fixture: ComponentFixture<StudentHomeworksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentHomeworksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentHomeworksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
