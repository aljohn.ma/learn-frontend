import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SubjectsComponent } from './subjects/subjects.component';
import { StudentExamsComponent } from './student-exams/student-exams.component';
import { StudentQuizzesComponent } from './student-quizzes/student-quizzes.component';
import { StudentHomeworksComponent } from './student-homeworks/student-homeworks.component';
import { QEHComponent } from './qeh/qeh.component';
import { QEHDataComponent } from './qeh-data/qeh-data.component';
import { TakeQEHComponent } from './take-qeh/take-qeh.component';
import { QehResultComponent } from './qeh-result/qeh-result.component';

const routes: Routes = [
  { path: 'subjects/:id', component: SubjectsComponent },
  { path: 'exams', component: StudentExamsComponent },
  { path: 'quizzes', component: StudentQuizzesComponent },
  { path: 'homeworks', component: StudentHomeworksComponent },
  { path: 'qeh/:id', component: QEHComponent },
  { path: 'qeh/:id/take', component: TakeQEHComponent },
  { path: 'qeh/:id/result', component: QehResultComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StudentRoutingModule { }

export const StudentRoutingComponents = [
  SubjectsComponent,
  StudentExamsComponent,
  StudentQuizzesComponent,
  StudentHomeworksComponent,
  QEHComponent,
  QEHDataComponent,
  TakeQEHComponent,
  QehResultComponent,
];
