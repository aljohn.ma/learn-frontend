import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StudentRoutingModule, StudentRoutingComponents } from './student-routing.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Countdown } from '../../pipes/countdown.pipe';
import { NgxPaginationModule } from 'ngx-pagination';
import { FormsModule } from '@angular/forms';
import { CreateArrayPipe } from '../../pipes/create-array.pipe';
import { QehResultComponent } from './qeh-result/qeh-result.component';
import { SharedModule } from '../../shared.module';

@NgModule({
  imports: [
    CommonModule,
    StudentRoutingModule,
    NgxDatatableModule,
    NgbModule,
    NgxPaginationModule,
    FormsModule,
    SharedModule
  ],
  declarations: [
    StudentRoutingComponents,
    Countdown,
    CreateArrayPipe,

  ]
})
export class StudentModule { }

