import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TakeQEHComponent } from './take-qeh.component';

describe('TakeQehComponent', () => {
  let component: TakeQEHComponent;
  let fixture: ComponentFixture<TakeQEHComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TakeQEHComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TakeQEHComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
