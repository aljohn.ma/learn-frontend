import { Component, OnInit, ViewChild, ChangeDetectionStrategy } from '@angular/core';
import { ApiService } from '../../../services/api.service';
import { GET_STUDENT_QEH_1, START_QEH, SUBMIT_QEH } from '../../../constants/urls';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { interval, Subscription } from 'rxjs';
import { QEH_TYPE } from '../../../constants/constants';

@Component({
  selector: 'app-take-qeh',
  templateUrl: './take-qeh.component.html',
  styleUrls: ['./take-qeh.component.scss'],
})
export class TakeQEHComponent implements OnInit {

  @ViewChild('content') content: NgbModal;
  @ViewChild('view_result_modal') result_modal: NgbModal;

  loading = true;
  time_remaining = '00:00:00';

  current_tab: QEH_TYPE;
  mc_page = 0;
  id_page = 0;
  tf_page = 0;
  en_page = 0;
  es_page = 0;

  TYPE = QEH_TYPE;
  tmr = interval(1000);
  hours = 0;
  minutes = 0;
  seconds = 0;
  timer_sub: Subscription;

  STUDENT_QEH: any;
  QEH: any;

  duration = 0;
  enumeration: any = [];
  identification: any = [];
  true_false: any = [];
  multiple_choice: any = [];
  essay: any = [];

  mc_answers = [];
  id_answers = [];
  tf_answers = [];
  en_answers = [];
  es_answers = [];

  constructor(
    private http: ApiService,
    private route: ActivatedRoute,
    private modal: NgbModal,
    private router: Router

  ) { }

  ngOnInit() {
    this.route.params.subscribe(
      par => {
        this.fetch(par['id']);
      }
    );
  }

  async fetch(id) {
    await this.http.get(GET_STUDENT_QEH_1(), id).toPromise().then(
      res => {

        this.handleResponse(res);
        if (this.STUDENT_QEH.status === 1) {
          this.open(this.content);
        }
      }
    );
  }

  handleResponse(res) {
    this.STUDENT_QEH = res['student_qeh'];
    if (this.STUDENT_QEH.status === 3) {
      this.view_result();
    } else {
      this.QEH = res['qeh'];
      this.enumeration = res['enum'];
      this.identification = res['id'];
      this.true_false = res['tf'];
      this.multiple_choice = res['mc'];
      this.essay = res['ess'];
      this.duration = res['time_remaining'];
      this.selectTab();
      if (this.STUDENT_QEH.status === 2) {
        this.startTimer();
      }
    }
    this.loading = false;
    this.modal.dismissAll();
  }


  open(content) {
    this.modal.open(content, { centered: true, backdrop: 'static', keyboard: false });
  }

  async start() {
    this.loading = true;
    await this.http.post(START_QEH(), { 'id': this.STUDENT_QEH.id }).toPromise().then(
      res => {
        this.handleResponse(res);
      }
    );
  }

  cancel() {
    this.modal.dismissAll();
    this.router.navigate(['../'], { relativeTo: this.route });
  }

  startTimer() {
    if (this.duration > 0) {

      this.hours = Math.floor(this.duration / 60);
      this.minutes = Math.floor(this.duration) % 60;
      this.seconds = Math.floor((this.duration - Math.floor(this.duration)) * 60);

      this.timer_sub = this.tmr.subscribe(
        s => {
          if (this.seconds === 0) {
            if (this.minutes === 0 && this.hours && this.seconds === 0) {
              this.minutes = 60;
              this.hours--;
            }
            this.seconds = 60;
            if (this.minutes > 0) { this.minutes--; }
          }
          this.seconds--;

          if (this.hours === 0 && this.minutes === 0 && this.seconds === 0) {
            this.time_remaining = `Time's up!`;
            this.timer_sub.unsubscribe();
            this.submit();
          }
        }
      );
    }
  }

  submit() {
    this.modal.dismissAll();
    const data = {
      id: this.STUDENT_QEH.id,
      qeh_id: this.QEH.id,
      enumeration: this.en_answers,
      identification: this.id_answers,
      multiple_choice: this.mc_answers,
      true_false: this.tf_answers,
      essay: this.es_answers
    };
    this.http.post(SUBMIT_QEH(), data).subscribe(
      () => {
        this.open(this.result_modal);
      }
    );
  }

  goto(type: QEH_TYPE, i: number, question) {
    console.log(i);

    if (i < 0) { i = 0; }

    switch (type) {
      case QEH_TYPE.id:
        this.id_page = (i >= this.identification.length ? this.identification.length - 1 : i);
        this.id_answers = this.addAnswer(this.id_answers, question);
        break;

      case QEH_TYPE.en:
        this.en_page = (i >= this.enumeration.length ? this.enumeration.length - 1 : i);
        this.addENAnswer(question);
        break;

      case QEH_TYPE.es:
        this.es_page = (i >= this.essay.length ? this.essay.length - 1 : i);
        this.es_answers = this.addAnswer(this.es_answers, question);
        break;

      case QEH_TYPE.tf:
        this.tf_page = (i >= this.true_false.length ? this.true_false.length - 1 : i);
        this.tf_answers = this.addAnswer(this.tf_answers, question);
        break;

      case QEH_TYPE.mc:
        this.mc_page = (i >= this.multiple_choice.length ? this.multiple_choice.length - 1 : i);
        this.mc_answers = this.addAnswer(this.mc_answers, question);
        break;

      default: break;
    }
  }

  addAnswer(answers, question) {
    const ans = this.buildAnswer(question);
    const idx = answers.indexOf(answers.find(x => x.question_id === ans.question_id));
    idx >= 0 ? answers[idx] = ans : answers.push(ans);
    return answers;
  }

  addENAnswer(question) {
    const ans = {
      question_id: question.id,
      question: question.question,
      answers: question.answers_arr,
    };

    const idx = this.en_answers.indexOf(this.en_answers.find(x => x.question_id === ans.question_id));
    idx >= 0 ? this.en_answers[idx] = ans : this.en_answers.push(ans);

  }

  buildAnswer(question) {
    const ans = {
      question_id: question.id,
      question: question.question,
      answer: question.answer,
    };

    return ans;
  }

  changeTab(type: QEH_TYPE) {
    this.current_tab = type;
  }

  selectTab() {
    if (this.QEH.has_multiple_choice) {
      this.current_tab = QEH_TYPE.mc;
      return;
    } else if (this.QEH.has_identification) {
      this.current_tab = QEH_TYPE.id;
      return;
    } else if (this.QEH.has_enumeration) {
      this.current_tab = QEH_TYPE.en;
      return;
    } else if (this.QEH.has_true_false) {
      this.current_tab = QEH_TYPE.tf;
      return;
    } else if (this.QEH.has_essay) {
      this.current_tab = QEH_TYPE.es;
      return;
    }
  }

  view_result() {
    this.router.navigate(['../result'], { relativeTo: this.route });
    this.modal.dismissAll();
  }

  isTimeUp() {
    return !this.hours && !this.minutes && !this.seconds;
  }

  getNextPrev(idx: number, length: number, next: boolean) {
    if (next) {
      return idx + 1 === length ? 'Save answer' : 'Next';
    }
    return 'Prev';
  }
}
