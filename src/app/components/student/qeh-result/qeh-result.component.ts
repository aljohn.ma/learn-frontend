import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from '../../../services/api.service';
import { GET_STUDENT_QEH_RESULT } from '../../../constants/urls';

@Component({
  selector: 'app-qeh-result',
  templateUrl: './qeh-result.component.html',
  styleUrls: ['./qeh-result.component.scss']
})
export class QehResultComponent implements OnInit {

  STUDENT_QEH: any;
  QEH: any;

  constructor(
    private http: ApiService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.route.params.subscribe(
      par => {
        this.fetch(par['id']);
      }
    );
  }

  async fetch(id) {
    await this.http.get(GET_STUDENT_QEH_RESULT(), id).toPromise().then(
      res => {
        this.STUDENT_QEH = res['student_qeh'];
        this.QEH = res['qeh'];
      }
    );
  }

}
