import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QehResultComponent } from './qeh-result.component';

describe('QehResultComponent', () => {
  let component: QehResultComponent;
  let fixture: ComponentFixture<QehResultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QehResultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QehResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
