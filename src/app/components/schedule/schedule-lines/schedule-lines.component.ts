import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Access } from '../../../models/access.model';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from '../../../services/api.service';
import { AuthenticationService } from '../../../services/authentication.service';
import { NgFlashMessageService } from 'ng-flash-messages';
import { plainToClass } from 'class-transformer';
import { API_SCHEDULES, API_SCHEDULE_LINES } from '../../../constants/urls';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { User } from '../../../models/user.model';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-schedule-lines',
  templateUrl: './schedule-lines.component.html',
  styleUrls: ['./schedule-lines.component.scss'],
})
export class ScheduleLinesComponent implements OnInit {

  @ViewChild(DatatableComponent) table: DatatableComponent;
  @ViewChild('content') conten: ElementRef;


  temp = [];
  schedule: any = {};
  loading = true;
  access: Access = null;
  obj: any = {};

  error: any = {};
  search = '';
  selected_section: any = {};

  subjects: any = [];
  rooms: any = [];
  teachers: User[];


  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private http: ApiService,
    private auth: AuthenticationService,
    private ngFlashMessageService: NgFlashMessageService,
    private modal: NgbModal,
  ) {
  }

  async ngOnInit() {
    await this.auth.getAccess('schedules').toPromise().then(
      res => {
        this.access = plainToClass(Access, res['access'] as Access);
      },
      () => { this.router.navigate(['/401']); }
    );

    if (this.access) {
      await this.fetch();
      this.new_obj();
    }
  }

  async fetch() {
    let id = 0;
    this.route.params.subscribe(
      par => {
        id = par['id'];
      }
    );
    await this.http.get(API_SCHEDULES(), id)
      .toPromise().then(
        res => {
          this.schedule = res['schedule'];
          this.teachers = plainToClass(User, res['teachers']);
          this.subjects = res['subjects'];
          this.rooms = res['rooms'];
          this.temp = this.schedule.lines;
          this.loading = false;
          this.new_obj();
          this.modal.dismissAll();
        },
        err => {
          this.handleError(err);
        }
      );
  }

  updateFilter() {
    this.loading = true;
    const val = this.search.toLowerCase();

    const temp = this.temp.filter(function (d) {
      return d.subject.subject_code.toLowerCase().indexOf(val) !== -1 || d.subject.subject_name.toLowerCase().indexOf(val) !== -1 || !val;
    });

    // update the rows
    this.schedule.lines = temp;
    // Whenever the filter changes, always go back to the first page
    this.table.recalculate();
    this.table.offset = 0;
    this.loading = false;
  }

  generateSY() {
    const dt = new Date();
    return `SY${dt.getFullYear()}-${dt.getFullYear() + 1}`;
  }

  add(content) {
    this.new_obj();
    // this.router.navigate([`schedules/${this.schedule.id}/add`]);
    this.open(content);
  }

  edit(obj, content) {
    this.obj = obj;
    this.open(content);
  }

  open(content) {
    this.modal.open(content, { centered: true, backdrop: 'static' });
  }

  dismiss() {
    this.modal.dismissAll();
  }

  delete(id) {
    this.loading = true;
    this.http.delete(API_SCHEDULE_LINES(), id).subscribe(
      () => {
        this.fetch();
        this.flash('Delete schedule line successful.', 'success');
        this.modal.dismissAll();
      }
    );
  }

  save() {
    this.loading = true;
    if (this.not_existing(this.obj)) {
      if (this.obj.id) {
        this.http.put(API_SCHEDULE_LINES(), this.obj.id, this.obj).subscribe(
          () => {
            this.fetch();
            this.flash('Update schedule line successful.', 'success');
          },
          err => {
            this.handleError(err);
          }
        );
        return;
      }
      this.http.post(API_SCHEDULE_LINES(), this.obj).subscribe(
        () => {
          this.fetch();
          this.flash('Schedule line created successfully.', 'success');
        },
        err => {
          this.handleError(err);
        }
      );

    } else {
      this.flash('Schedule code existing', 'error');
      this.loading = false;
    }
  }

  handleError(error) {
    this.error = error.error.errors;
    this.flash(error.error.message, 'danger');
    this.loading = false;
    console.log(error);
    if (error.status === 404) { this.router.navigate(['/schedules']); }
  }

  cancel() {
    this.new_obj();
    this.dismiss();
  }

  new_obj() {
    this.obj = {
      sh_id: this.schedule.id,
      teacher_id: null,
      subject_id: null,
      room_id: null,
    };
    this.error = {};
  }

  private not_existing(obj) {

    let existing = this.schedule.lines.find(x => x.subject_id === obj.subject_id);
    if (obj.id) {
      existing = this.schedule.lines.find(x => x.subject_id === obj.subject_id && x.id !== obj.id);
    }
    return !existing;
  }

  flash(message, type) {
    const flashMessage = {
      messages: [message],
      timeout: 2000,
      type: `${type} p-2 mb-0 border-0 rounded-0`,
      dismissible: false
    };

    this.ngFlashMessageService.showFlashMessage(flashMessage);
  }
  getTeachername(row) {
    const t = plainToClass(User, row as User);
    return t.getFullname();
  }
}
