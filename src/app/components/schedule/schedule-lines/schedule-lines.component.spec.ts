import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScheduleLinesComponent } from './schedule-lines.component';

describe('ScheduleLinesComponent', () => {
  let component: ScheduleLinesComponent;
  let fixture: ComponentFixture<ScheduleLinesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScheduleLinesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScheduleLinesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
