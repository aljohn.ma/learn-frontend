import { Component, OnInit, ViewChild } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Access } from '../../models/access.model';
import { ApiService } from '../../services/api.service';
import { AuthenticationService } from '../../services/authentication.service';
import { NgFlashMessageService } from 'ng-flash-messages';
import { plainToClass } from 'class-transformer';
import { API_SCHEDULES } from '../../constants/urls';
import { Router } from '@angular/router';
import { FlashUtil } from '../../utils/flash.util';
import { FLASH_LABEL } from '../../constants/config';

@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.scss']
})
export class ScheduleComponent implements OnInit {

  @ViewChild(DatatableComponent) table: DatatableComponent;

  temp = [];
  schedules: any = [];
  loading = true;
  access: Access = null;
  obj: any = {};

  error: any = {};
  search = '';
  sections: any = [];
  selected_section: any = {};

  constructor(
    private router: Router,
    private http: ApiService,
    private auth: AuthenticationService,
    private ngFlashMessageService: NgFlashMessageService,

  ) { }

  async ngOnInit() {
    await this.auth.getAccess('schedules').toPromise().then(
      res => {
        this.access = plainToClass(Access, res['access'] as Access);
      },
      () => { this.router.navigate(['/401']); }
    );

    if (this.access) {
      this.fetch();
      this.new_obj();
    }
  }

  fetch() {
    this.http.get_list(API_SCHEDULES())
      .subscribe(
        res => {
          this.sections = res['sections'];
          this.schedules = res['schedules'];
          this.temp = [...this.schedules];
          this.loading = false;
        },
      );
  }

  updateFilter() {
    this.loading = true;
    const val = this.search.toLowerCase();

    const temp = this.temp.filter(function (d) {
      return d.schedule_code.toLowerCase().indexOf(val) !== -1 || d.description.toLowerCase().indexOf(val) !== -1 || !val;
    });

    // update the rows
    this.schedules = temp;
    // Whenever the filter changes, always go back to the first page
    this.table.recalculate();
    this.table.offset = 0;
    this.loading = false;
  }

  generateSY() {
    const dt = new Date();
    return `SY${dt.getFullYear()}-${dt.getFullYear() + 1}`;
  }
  view(row) {
    this.router.navigate([`schedules/${row.id}`]);
  }
  add() {
    this.new_obj();
  }

  edit(obj) {
    this.obj = obj;
    this.showSection();
  }

  delete(id) {
    this.loading = true;
    this.http.delete(API_SCHEDULES(), id).subscribe(
      () => {
        this.fetch();
        this.flash('Delete schedule successful.', 'success');
      }
    );
  }

  save() {
    this.loading = true;
    if (this.not_existing(this.obj)) {
      if (this.obj.id) {
        this.http.put(API_SCHEDULES(), this.obj.id, this.obj).subscribe(
          () => {
            this.fetch();
            this.new_obj();

            this.flash('Update schedule successful.', 'success');
          },
          err => {
            this.handleError(err);
            this.loading = false;
          }
        );
        return;
      }
      this.http.post(API_SCHEDULES(), this.obj).subscribe(
        () => {
          this.fetch();
          this.new_obj();
          this.flash('Schedule created successfully.', 'success');
        },
        err => {
          this.handleError(err);
          this.loading = false;

        }
      );
    } else {
      this.flash('Schedule code existing', 'error');
      this.loading = false;
    }
  }

  handleError(error) {
    this.error = error.error.errors;
    this.flash(error.error.message, 'danger');
  }

  cancel() {
    this.new_obj();
  }

  new_obj() {
    this.obj = { school_year: this.generateSY(), section_id: null };
    this.error = {};
  }

  private not_existing(obj) {
    let existing = this.schedules.find(x => x.schedule_code.toLowerCase() === obj.schedule_code.toLowerCase());
    if (obj.id) {
      existing = this.schedules.find(x => x.schedule_code.toLowerCase() === obj.schedule_code.toLowerCase() && x.id !== obj.id);
    }
    return !existing;
  }
  showSection() {
    if (this.obj.section_id) {
      this.selected_section = this.sections.find(x => x.id === this.obj.section_id);
    }
  }
  flash(message, type) {
    const flashMessage = {
      messages: [message],
      timeout: 2000,
      type: FLASH_LABEL(type),
      dismissible: false
    };

    this.ngFlashMessageService.showFlashMessage(flashMessage);
  }


}
