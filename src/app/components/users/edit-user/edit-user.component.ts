import { Component, OnInit } from '@angular/core';
import { User } from '../../../models/user.model';
import { ApiService } from '../../../services/api.service';
import { Router, ActivatedRoute } from '@angular/router';
import { plainToClass } from 'class-transformer';
import { NgFlashMessageService } from 'ng-flash-messages';
import { API_USERS } from '../../../constants/urls';
import { AuthenticationService } from '../../../services/authentication.service';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.scss']
})
export class EditUserComponent implements OnInit {

  access = null;

  user: User = null;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private http: ApiService,
    private flash: NgFlashMessageService,
    private auth: AuthenticationService

  ) { }


  async ngOnInit() {

    await this.auth.getAccess('users').toPromise().then(
      res => {
        this.access = res['access'];
        if (this.access && this.access.can_edit) {
          this.fetch();
        } else {
          this.router.navigate(['/401']);
        }
      }
    );
  }


  async fetch() {
    await this.http.get(API_USERS(), this.getID()).toPromise().then(
      res => {
        this.user = plainToClass(User, res['data'] as User);
      },
      err => {
        this.handleError(err);
      }
    );
  }

  private getID(): number {
    let ret = 0;
    this.route.params.subscribe(
      par => {
        ret = par['id'];
      }
    );
    return ret;
  }

  private handleError(err: any) {
    const flashMessage = {
      messages: [err.error.error],
      timeout: 2000,
      type: 'error',
      dismissible: false
    };
    this.router.navigate(['/users']);
    this.flash.showFlashMessage(flashMessage);
  }

}
