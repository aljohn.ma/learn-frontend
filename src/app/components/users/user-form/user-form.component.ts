import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { User } from '../../../models/user.model';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from '../../../services/api.service';
import {
  API_USERS,
  GET_APP_FEATURES,
  API_COUNTRIES,
  GET_STATES,
  GET_CITIES,
  SAVE_STATE,
  SAVE_CITY,
  SAVE_POSTAL,
  GET_POSTAL_CODES,
  API_LEVELS,
  GET_SECTIONS_BY_LEVEL,
  GET_SCHEDULES_BY_SECTION,
  GET_USERS_BY_TYPE
} from '../../../constants/urls';
import { USER } from '../../../constants/constants';
import { Feature } from '../../../models/feature.model';
import { plainToClass } from 'class-transformer';
import { NgbPopover, NgbDateAdapter, NgbDateNativeAdapter, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgFlashMessageService } from 'ng-flash-messages';
import { FLASH_LABEL } from '../../../constants/config';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss'],
  providers: [{ provide: NgbDateAdapter, useClass: NgbDateNativeAdapter }]
})
export class UserFormComponent implements OnInit {

  @ViewChild('addFeaturePop') public popover: NgbPopover;
  @ViewChild('addCountryPop') public addCountry: NgbPopover;
  @ViewChild('addStatePop') public addState: NgbPopover;
  @ViewChild('addCityPop') public addCity: NgbPopover;
  @ViewChild('addPostalCodePop') public addPostal: NgbPopover;
  @ViewChild('profilePicModal') public profilePicModal: ElementRef;

  @Input() user: User;

  bsConfig: Partial<BsDatepickerConfig>;
  app_features: Feature[];

  obj: any = {};
  countries: any = [];
  states: any = [];
  postal_codes: any = [];
  cities: any = [];

  error: any = {};

  levels: any = [];
  sections: any = [];
  schedules: any = [];
  students: any;

  saving = false;

  imageChangedEvent: any = '';
  croppedImage: any = '';
  cropperReady = false;

  loading = true;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private http: ApiService,
    private ngFlashMessageService: NgFlashMessageService,
    private modal: NgbModal,
  ) { }

  async ngOnInit() {

    this.bsConfig = Object.assign({}, { containerClass: 'theme-blue', dateInputFormat: 'MM/DD/YYYY' });

    await this.getAppFeatures();
    await this.getCountries();
    await this.getLevels();

    if (this.user.id) {
      this.user.birthdate = new Date(this.user.birthdate);
      await this.getStates();
      await this.getCities();
      await this.getPostalCodes();
      if (this.user.user_type === USER.STUDENT) {
        await this.getSections();
        await this.getSchedules();
      }
    }
    this.loading = false;
  }

  private getAppFeatures() {

    if (this.user.user_type === USER.STAFF ||
      this.user.user_type === USER.TEACHER) {
      this.http.get_list(GET_APP_FEATURES()).toPromise().then(res => {
        this.app_features = plainToClass(Feature, res['data']);
      });
    } else if (this.user.user_type === USER.PARENT) {
      this.getStudents();
    }
  }

  private getLevels() {
    if (this.user.user_type === USER.STUDENT) {
      this.http.get_list(API_LEVELS()).toPromise().then(res => {
        this.levels = res;
      });
    }
  }
  getSections() {
    if (this.user.student.level_id) {
      this.http.get(GET_SECTIONS_BY_LEVEL(), this.user.student.level_id).subscribe(
        res => {
          this.sections = res;
        }
      );
    }
  }
  getSchedules() {
    if (this.user.student.section_id) {
      this.http.get(GET_SCHEDULES_BY_SECTION(), this.user.student.section_id).subscribe(
        res => {
          this.schedules = res;
        }
      );
    }
  }
  getStudents() {
    this.http.get_list(GET_USERS_BY_TYPE(USER.STUDENT)).subscribe(
      res => {
        this.students = plainToClass(User, res);
      }
    );
  }

  save() {
    this.saving = true;

    if (this.user.id) {
      this.update();
    } else { this.create(); }

  }

  create() {
    this.http.post(API_USERS(), this.user)
      .subscribe(
        res => {
          this.flash(res['data'], 'success');
          this.saving = false;
          this.redirect();

        },
        err => {
          this.handleError(err);
          this.flash(err.error['message'], 'danger');
          this.saving = false;
        }
      );

  }

  update() {
    this.http.put(API_USERS(), this.user.id, this.user)
      .subscribe(
        res => {
          this.flash(res['data'], 'success');
          this.saving = false;
          this.redirect();
        },
        err => {
          this.handleError(err);
          this.flash(err.error['message'], 'danger');
          this.saving = false;
        }
      );
  }

  removeFeature(feature: Feature) {
    if (feature.pivot.id > 0) {
      this.user.removed_access.push(feature);
    }
    this.user.user_access.splice(this.user.user_access.indexOf(feature), 1);
  }

  check_un(e, feature: Feature) {
    const existing_feature = this.user.user_access.find(x => x.id === feature.id);
    if (e.target.checked && !existing_feature) {
      this.user.user_access
        .push(
          feature
        );
      if (feature.pivot.id > 0) {
        this.user.removed_access.splice(this.user.removed_access.indexOf(feature), 1);
      }
    } else {
      if (existing_feature) { this.removeFeature(existing_feature); }
    }
  }

  isFeatureSelected(feature) {
    return this.user.user_access.find(x => x.id === feature.id);
  }


  async getCountries() {
    await this.http.get_list(API_COUNTRIES()).toPromise().then(
      res => {
        this.countries = res['countries'];
      }
    );
  }
  async getStates() {
    if (this.user.country_id) {
      await this.http.get(GET_STATES(), this.user.country_id).toPromise().then(
        res => {
          if (res) {
            this.states = res['states'];
          } else {
            this.states = [];
            this.addState.open();
          }
        }
      );
    }
  }
  getCities() {
    if (this.user.state_id) {
      this.http.get(GET_CITIES(), this.user.state_id).subscribe(
        res => {
          if (res) {
            this.cities = res['cities'];
          } else {
            this.cities = [];
            this.addCity.open();
          }
        }
      );
    }
  }
  getPostalCodes() {
    if (this.user.city_id) {
      this.http.get(GET_POSTAL_CODES(), this.user.city_id).subscribe(
        res => {
          if (res) {
            this.postal_codes = res['postal_codes'];
          } else {
            this.postal_codes = [];
            this.addPostal.open();
          }
        }
      );
    }
  }

  saveCountry() {

    if (!this.countries.find(e => e.country_name.toLowerCase() === this.obj.name.toLowerCase())) {

      const country = {
        'country_name': this.obj.name,
        'country_code': this.obj.code,
        'phone_code': this.obj.phone_code,
      };

      this.http.post(API_COUNTRIES(), country).subscribe(
        res => {
          this.obj = {};
          this.getCountries();
          this.addCountry.close();
          this.user.country_id = res['id'];
          this.getStates();
        }
      );
    }
  }

  saveState() {

    if (!this.states.find(e => e.state_name.toLowerCase() === this.obj.name.toLowerCase())) {

      const state = {
        'country_id': this.user.country_id,
        'state_name': this.obj.name,
      };

      this.http.post(SAVE_STATE(), state).subscribe(
        res => {
          this.obj = {};
          this.getStates();
          this.addState.close();
          this.user.state_id = res['id'];
          this.getCities();
        }
      );
    }
  }

  saveCity() {

    if (!this.cities.find(e => e.city_name.toLowerCase() === this.obj.name.toLowerCase())) {

      const city = {
        'state_id': this.user.state_id,
        'city_name': this.obj.name,
      };

      this.http.post(SAVE_CITY(), city).subscribe(
        res => {
          this.obj = {};
          this.getCities();
          this.addCity.close();
          this.user.city_id = res['id'];
          this.getPostalCodes();
        }
      );
    }
  }

  savePostaCode() {

    if (!this.postal_codes.find(e => e.postal_code.toLowerCase() === this.obj.code.toLowerCase())) {

      const postal = {
        'city_id': this.user.city_id,
        'postal_code': this.obj.code,
      };

      this.http.post(SAVE_POSTAL(), postal).subscribe(
        res => {
          this.obj = {};
          this.getPostalCodes();
          this.addPostal.close();
          this.user.postal_code_id = res['id'];
        }
      );
    }
  }

  handleError(error) {

    this.error = error.error.errors;
    if (this.user.user_type === USER.STUDENT) {
      this.error['student_code'] = this.error['student.student_code'];
      this.error['student_number'] = this.error['student.student_number'];
      this.error['level_id'] = this.error['student.level_id'];
      this.error['section_id'] = this.error['student.section_id'];
      this.error['schedule_id'] = this.error['student.schedule_id'];

    } else if (this.user.user_type === USER.PARENT) {
      this.error['pg_code'] = this.error['parent.pg_code'];
      this.error['student_id'] = this.error['parent.student_id'];
      this.error['pg_relationship'] = this.error['parent.pg_relationship'];
    }
    console.log(this.error);

  }

  flash(message, type) {
    const flashMessage = {
      messages: [message],
      timeout: 2000,
      type: FLASH_LABEL(type),
      dismissible: false
    };

    this.ngFlashMessageService.showFlashMessage(flashMessage);
  }

  cancel() {
    this.redirect();
  }

  private redirect() {
    this.route.params.subscribe(
      par => {
        this.router.navigate([`/users/${par['type']}`]);
      }
    );
  }

  fileChangeEvent(event: any): void {

    this.imageChangedEvent = event;
    this.uploadProfilePicture(this.profilePicModal);
  }

  imageCroppedBase64(image: string) {
    this.user.profile_path = image;
    this.user.profile_pic = image;

  }

  imageLoaded() {
    this.cropperReady = true;
  }

  loadImageFailed() {
    console.log('Load failed');
  }

  changeProfilePic() {
    this.uploadProfilePicture(this.profilePicModal);
  }

  uploadProfilePicture(content) {
    if (this.imageChangedEvent.target.files.length) {
      this.modal.open(content, { centered: true, backdrop: 'static' });
    }
  }

  createUsername() {
    switch (this.user.user_type) {
      case USER.PARENT:
        this.user.username = this.user.parent.pg_code;
        return;
      case USER.STUDENT:
        this.user.username = this.user.student.student_number;
        return;
      default: return '';
    }
  }
}
