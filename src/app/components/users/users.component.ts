import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { Router } from '@angular/router';
import { DatatableComponent } from '@swimlane/ngx-datatable';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  user_filter = 0;

  @ViewChild(DatatableComponent) table: DatatableComponent;

  constructor(
    private http: ApiService,
    private router: Router
  ) { }

  ngOnInit() {

  }

}
