import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersRoutingModule, UsersRoutingComponents } from './users-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { BsDatepickerModule, DatepickerModule } from 'ngx-bootstrap/datepicker';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { ImageCropperModule } from 'ngx-image-cropper';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../../shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    UsersRoutingModule,
    NgxDatatableModule,
    BsDatepickerModule.forRoot(),
    DatepickerModule.forRoot(),
    NgxMaterialTimepickerModule.forRoot(),
    NgbModule,
    ImageCropperModule,
    SharedModule
  ],
  declarations: [UsersRoutingComponents]
})
export class UsersModule { }
