import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { GET_USERS_BY_TYPE, API_USERS, ENABLE_DISABLE_USER } from '../../../constants/urls';
import { ApiService } from '../../../services/api.service';
import { plainToClass } from 'class-transformer';
import { User } from '../../../models/user.model';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Router } from '../../../../../node_modules/@angular/router';
import { UserConfig } from '../user.config';
import { NgFlashMessageService } from 'ng-flash-messages';
import { NgbPopover } from '@ng-bootstrap/ng-bootstrap';
import { Access } from '../../../models/access.model';
import { AuthenticationService } from '../../../services/authentication.service';
import { FLASH_LABEL } from '../../../constants/config';


@Component({
  selector: 'app-data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.scss']
})
export class DataTableComponent implements OnInit {

  @Input() user_filter = 0;
  @ViewChild(DatatableComponent) table: DatatableComponent;
  @ViewChild('deletePop') public popover: NgbPopover;

  temp = [];
  users: any;
  loading = true;

  access: Access = null;

  constructor(
    private http: ApiService,
    private router: Router,
    private flashMessage: NgFlashMessageService,
    private auth: AuthenticationService
  ) { }

  async ngOnInit() {

    await this.auth.getAccess('users').toPromise().then(
      res => {
        this.access = plainToClass(Access, res['access'] as Access);
      }
    );

    if (this.access) {
      this.fetch();
    }
  }

  fetch() {
    this.http.get_list(GET_USERS_BY_TYPE(this.user_filter))
      .subscribe(
        res => {
          this.users = plainToClass(User, res);
          this.temp = [...this.users];
          this.loading = false;
        },
      );
  }

  updateFilter(event) {

    const val = event.target.value.toLowerCase();

    const temp = this.temp.filter(function (d) {
      return d.getFullname().toLowerCase().indexOf(val) !== -1 || !val;
    });

    // update the rows
    this.users = temp;
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }

  add() {
    this.router.navigate([`${this.router.url}/add`]);
  }

  edit(id: number, user_type: number) {
    this.router.navigate([`users/${UserConfig.getUserURL(user_type)}/edit/${id}`]);
  }

  delete(id) {
    this.http.delete(API_USERS(), id)
      .subscribe(
        res => {
          this.flash(res['data'], 'success');
          this.fetch();
          this.popover.close();
        },
        err => {
          this.handleError(err);
        }
      );
  }
  enable(user: User) {
    this.loading = true;
    const data = { 'is_active': !user.is_active };
    this.http.put(ENABLE_DISABLE_USER(), user.id, data).subscribe(
      () => {
        user.is_active = !user.is_active;
        this.loading = false;

      },
      err => {
        this.handleError(err);

      });
  }

  private handleError(err) {
    this.loading = false;
    this.flash(err.error.error, 'danger');
    if (err.status === 401) {
      this.router.navigate(['/401']);
    }
  }
  flash(message, type) {
    const flashMessage = {
      messages: [message],
      timeout: 30000,
      type: FLASH_LABEL(type),
      dismissible: false
    };
    this.flashMessage.showFlashMessage(flashMessage);
  }
}
