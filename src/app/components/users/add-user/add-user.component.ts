import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '../../../../../node_modules/@angular/router';
import { UserConfig } from '../user.config';
import { User } from '../../../models/user.model';
import { Access } from '../../../models/access.model';
import { AuthenticationService } from '../../../services/authentication.service';
import { plainToClass } from 'class-transformer';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss']
})
export class AddUserComponent implements OnInit {

  access: Access = null;
  user_type = 0;
  user: User = new User();

  constructor(
    private auth: AuthenticationService,
    private route: ActivatedRoute,
    private router: Router
  ) {
  }

  ngOnInit() {

    this.auth.getAccess('users').toPromise().then(
      res => {
        this.getUserType();
        this.access = plainToClass(Access, res['access'] as Access);
        if (this.access && !this.access.can_add) {
          this.router.navigate(['/401']);
        }
      });
  }

  getUserType() {
    this.route.params.subscribe(
      par => {
        this.user.user_type = UserConfig.getUsertype(par['type']);
      }
    );
  }

}
