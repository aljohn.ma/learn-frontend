import { Component, OnInit, Input } from '@angular/core';
import { USER } from '../../../constants/constants';

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.scss']
})
export class StudentComponent implements OnInit {

  user_type = USER.STUDENT;

  constructor() { }

  ngOnInit() {
  }


}
