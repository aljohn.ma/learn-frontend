import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditUserComponent } from './edit-user/edit-user.component';
import { AddUserComponent } from './add-user/add-user.component';
import { StaffComponent } from './staff/staff.component';
import { TeachersComponent } from './teachers/teachers.component';
import { ParentsComponent } from './parents/parents.component';
import { StudentComponent } from './student/student.component';
import { AllUsersComponent } from './all-users/all-users.component';
import { UsersComponent } from './users.component';
import { DataTableComponent } from './data-table/data-table.component';
import { UserFormComponent } from './user-form/user-form.component';

const routes: Routes = [
  { path: '', component: AllUsersComponent },
  { path: 'students', component: StudentComponent },
  { path: 'parents', component: ParentsComponent },
  { path: 'teachers', component: TeachersComponent },
  { path: 'staff', component: StaffComponent },
  { path: ':type/add', component: AddUserComponent },
  { path: ':type/edit/:id', component: EditUserComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule { }

export const UsersRoutingComponents = [
  AllUsersComponent,
  StudentComponent,
  ParentsComponent,
  TeachersComponent,
  StaffComponent,
  AddUserComponent,
  EditUserComponent,
  DataTableComponent,
  UserFormComponent,
];
