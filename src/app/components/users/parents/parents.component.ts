import { Component, OnInit } from '@angular/core';
import { USER } from '../../../constants/constants';

@Component({
  selector: 'app-parents',
  templateUrl: './parents.component.html',
  styleUrls: ['./parents.component.scss']
})
export class ParentsComponent implements OnInit {

  user_type = USER.PARENT;

  constructor() { }

  ngOnInit() {
  }

}
