
import { USER } from '../../constants/constants';

export class UserConfig {
    static getUsertype(str: string) {
        switch (str) {
            case 'staff':
                return USER.STAFF;
            case 'students':
                return USER.STUDENT;
            case 'teachers':
                return USER.TEACHER;
            case 'parents':
                return USER.PARENT;
            default:
                return 0;
        }
    }
    static getUsertypeStr(typ: USER) {
        switch (typ) {
            case USER.STAFF:
                return 'Staff';
            case USER.STUDENT:
                return 'Student';
            case USER.TEACHER:
                return 'Teacher';
            case USER.TEACHER:
                return 'Parent';
            default:
                return null;
        }
    }
    static getUserURL(typ: USER) {
        switch (typ) {
            case USER.STAFF:
                return 'staff';
            case USER.STUDENT:
                return 'students';
            case USER.TEACHER:
                return 'teachers';
            case USER.PARENT:
                return 'parents';
            default:
                return null;
        }
    }
}
