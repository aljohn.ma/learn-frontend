import { Component, OnInit } from '@angular/core';
import { USER } from '../../../constants/constants';

@Component({
  selector: 'app-teachers',
  templateUrl: './teachers.component.html',
  styleUrls: ['./teachers.component.scss']
})
export class TeachersComponent implements OnInit {

  user_type = USER.TEACHER;


  constructor() { }

  ngOnInit() {
  }

}
