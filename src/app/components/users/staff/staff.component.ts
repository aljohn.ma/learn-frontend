import { Component, OnInit } from '@angular/core';
import { USER } from '../../../constants/constants';

@Component({
  selector: 'app-staff',
  templateUrl: './staff.component.html',
  styleUrls: ['./staff.component.scss']
})
export class StaffComponent implements OnInit {

  user_type = USER.STAFF;

  constructor() { }

  ngOnInit() {
  }

}
