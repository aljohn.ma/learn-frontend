import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../services/authentication.service';
import { TokenService } from '../../services/token.service';
import { LoginStatusService } from '../../services/login.service';
import { Router, ActivatedRoute } from '@angular/router';
import { User } from '../../models/user.model';
import { ApiService } from '../../services/api.service';
import { GET_AUTH_USER } from '../../constants/urls';
import { plainToClass } from 'class-transformer';
import { USER } from '../../constants/constants';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  is_logged_in = false;
  user_access: any;
  setup_menu: any;
  main_menu: any;
  user: User;
  subjects = [];

  constructor(
    private auth: AuthenticationService,
    private token: TokenService,
    private http: ApiService,
    private router: Router,
    private login_status: LoginStatusService
  ) { }

  ngOnInit() {

    this.auth.authenticate();
    this.login_status.currentStatus.subscribe(
      res => {
        this.is_logged_in = res['status'];
        this.user_access = res['access'];
        this.main_menu = res['main_menu'];
        this.setup_menu = res['setup_menu'];
        this.user = res['user'];
      }
    );

    if (this.is_logged_in = this.token.is_valid()) {
      this.http.get_list(GET_AUTH_USER()).subscribe(
        res => {
          this.user = plainToClass(User, res['user'] as User);
          this.user_access = res['access'];
          if (this.user.user_type === USER.TEACHER) {
            this.subjects = res['subjects'];
          } else if (this.user.user_type === USER.STUDENT) {
            this.user.student = res['student_info'];
            this.subjects = this.user.student.schedule.lines;
          }
          this.extractAccess();
        }
      );
    }
  }

  extractAccess() {
    this.main_menu = this.user_access.filter(x => x.dropdown_id === 0);
    this.setup_menu = this.user_access.filter(x => x.dropdown_id === 1);
  }

  viewProfile() {
    this.router.navigate(['/profile']);
  }

  logout() {
    this.auth.logoutUser()
      .subscribe(
        () => {
          this.token.remove();
          this.is_logged_in = false;
          this.router.navigate(['login']);
        },
        error => { console.log(error); }
      );
  }
}
